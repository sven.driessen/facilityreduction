"""
All of the main algorithms. Inputs are always parts of an instance. See instance def. in instance.py
"""

import numpy as np
import util
from tqdm import tqdm

#================ INSTANCE OPTIMIZATION =================

def get_essential_facilities(F,d,d_max):
    """
    returns:
        essentials: list containing the set of essential facilities for each level i
    """
    n,m = d.shape
    k = len(d_max)
    reach = util.compute_reachability_matrix(F,d,d_max)
    X = []
    for i in range(k):
        facilities=[]
        for l in range(n):
            cover = np.flatnonzero(reach[l,:,i])
            if len(cover)==1:
                facilities.append(cover[0])
        X.append(set(facilities))
    
    for i in reversed(range(k-1)):
        X[i].update(X[i+1])
    
    X = [list(X[i]) for i in range(k)]
    return X
    
def compute_coverage_classes(F,d,d_max):
    """
    returns:
        classes: A dict containing a (key,value) pair for each coverage class, where
            key = (F_0(v),...,F_k(v)) and
            value = [v] (the coverage class of v)
    """
    n,m = d.shape
    k = len(d_max)
    reach = util.compute_reachability_matrix(F,d,d_max)
    classes = dict()
    for l in range(n):
        cover = []
        for i in range(k):
            cover.append(tuple([j for j in range(m) if reach[l,j,i]]))
        cover = tuple(cover)
        if cover in classes:
            classes[cover].append(l)
        else:
            classes[cover] = [l]

    return classes

def reduce_coverage_classes(V,F,d,d_max):
    """
    returns:
        V_new: reduced locations
        d_new: reduced distance table
    """
    classes = compute_coverage_classes(F,d,d_max)
    representatives = []
    V_new = V.copy()
    for locations in classes.values():
        V_new[locations[0],2]=np.sum([V_new[l,2] for l in locations])
        representatives.append(locations[0])
    V_new = V_new[representatives,:]
    d_new = d[representatives,:]
    return V_new, d_new

def compute_nondominated_facilities(F,d,d_max):
    """
    returns:
        F_new: nondominated facilities
        d_new: reduced distance table
    """
    n,m = d.shape
    k = len(d_max)
    reach = util.compute_reachability_matrix(F,d,d_max)
    covers = {}
    for j in range(m):
        cover = []
        for i in range(k):
            cover.append(frozenset({l for l in range(n) if reach[l,j,i]}))
        cover=tuple(cover)
        for key in covers.keys():
            dominated = True
            dominates = True
            for i in range(k):
                if not cover[i].issubset(key[i]):
                    dominated = False
                if not cover[i].issuperset(key[i]):
                    dominates = False
            if dominated:
                break
            if dominates:
                covers.pop(key)
                covers[cover]=j
                break
        covers[cover]=j
    nondominated = list(covers.values())
    F_new = F[nondominated,:]
    d_new = d[:,nondominated]
    return F_new, d_new

def compute_nondominated_locations(V,F,d,d_max):
    """
    ONLY FOR VALID INSTANCES WITH ALPHA=0!
    returns:
        V_new: nondominated locations
        d_new: reduced distance table
    """
    n,m = d.shape
    k = len(d_max)
    reach = util.compute_reachability_matrix(F,d,d_max)
    V_new = V.copy()
    covers = {}
    for l in range(n):
        cover = []
        for i in range(k):
            cover.append(frozenset({j for j in range(m) if reach[l,j,i]}))
        cover = tuple(cover)
        is_new = True
        for key in covers.keys():
            dominated = True
            dominates = True
            for i in range(k):
                if not cover[i].issubset(key[i]):
                    dominates = False
                if not cover[i].issuperset(key[i]):
                    dominated = False
            if dominated:
                is_new = False
                V_new[covers[key],2]+=V_new[l,2]
                V_new[l,2]=0
                break
            if dominates:
                is_new = False
                V_new[l,2]+=V_new[covers[key],2]
                V_new[covers[key],2]=0
                covers.pop(key)
                covers[cover]=l
                break
        if is_new:
            covers[cover]=l
    nondominated = [l for l in range(n) if V_new[l,2]!=0]
    V_new = V_new[nondominated,:]
    d_new = d[nondominated,:]
    return V_new, d_new

#========================== PRE-CALCULATING REDUCED LOCATIONS (STATIC REDUCTION)===================================

def delete_anchors(V,F,d,d_max,alpha):
    """
    returns:
        V_new: reduced location set
        d_new: reduced distance table
        alpha_new = 0
    """
    n = d.shape[0]
    k = len(d_max)
    pop = np.sum(V[:,2])
    margin = alpha*pop
    reach = util.compute_reachability_matrix(F,d,d_max)

    candidates = [(l,i) for l in range(n) for i in range(k) if V[l,2]<=margin]
    candidates = sorted(candidates,key= lambda x: (np.sum(reach[x[0],:,x[1]]),-x[1],V[x[0],2]),reverse=True)
    remove=[]
    while len(candidates)!=0:
        l_star = candidates.pop()[0]
        if V[l_star,2]>margin or l_star in remove:
            continue
        remove.append(l_star)
        margin -= V[l_star,2]
        
    

    V_new = np.delete(V,remove,0)
    d_new = np.delete(d,remove,0)
    
    return V_new, d_new, 0


def area_sweep(V,F,d,d_max,alpha):
    """
returns:
    V_new: reduced location set
    d_new: reduced distance table
    alpha_new = 0
"""
    n,m = d.shape
    k = len(d_max)
    reach = util.compute_reachability_matrix(F,d,d_max)

    margin = alpha*np.sum(V[:,2])
    #remove uncoverable locations first
    remove = list(np.flatnonzero(np.invert(np.all(np.any(reach,axis=1),axis=1))))
    margin = alpha*np.sum(V[:,2]) - np.sum(V[remove,2])
    candidates = sorted([l for l in range(n) if not l in remove],key = lambda l: V[l,2])
    for l in candidates:
        if V[l,2]>margin:
            break
        else:
            remove.append(l)
            margin -= V[l,2]
            

    V_new = np.delete(V,remove,0)
    d_new = np.delete(d,remove,0)
    
    return V_new, d_new, 0


#===================================== FINAL ALGORITHMS ==========================================

def top_down_cover(V,F,d,d_max,alpha,X_start = None):
    """
    special input X_start : list of lists of facility indices, solution headstart 
    returns : solution X = [X_1,...,X_k] with X_i being lists of indices of solution facilities
    """
    #print("computing top-down cover")
    #initializations
    n,m = d.shape
    k = len(d_max)
    reach = util.compute_reachability_matrix(F,d,d_max)
    pop = np.sum(V[:,2])
    pop_min = (1-alpha)*pop
    X = [[] for i in range(k)]
    covered = [[] for i in range(k)]
    if X_start != None:
        X = X_start
        for i in range(k):
            covered[i]=list(np.flatnonzero(np.any(reach[:,X[i],i],axis=1)))
            reach[:,X[i],i] = 0
            reach[covered[i],:,i] = 0

    C = np.sum(reach,axis=0)
    
    for i in reversed(range(k)):
        #compute partial solution X_i
        candidates = np.flatnonzero(F[:,2]>=i)
        indices = list(reversed(range(i+1)))
        while np.sum(V[covered[i],2]) < pop_min:
            #find heuristic optimum
            j_star = max(candidates,key=lambda j: tuple(C[j,indices]))
            for i_prime in range(i+1):
                #add facility to solution
                X[i_prime].append(j_star)
                #mark locations as covered
                locations = np.flatnonzero(reach[:,j_star,i_prime])
                covered[i_prime].extend(locations)
                #update reach matrix
                reach[:,j_star,i_prime]=0
                reach[locations,:,i_prime]=0
            #update coverage matrix
            C=np.sum(reach,axis=0)
    return X
    

def bottom_up_cover(V,F,d,d_max,alpha,X_start = None):
    """
    special input X_start : list of lists of facility indices, solution headstart 
    returns : solution X = [X_1,...,X_k] with X_i being lists of indices of solution facilities
    """
    #print("computing bottom-up cover")
    #initializations
    n,m = d.shape
    k = len(d_max)
    pop = np.sum(V[:,2])
    pop_min = (1-alpha)*pop
    X = [[] for i in range(k)]
    reach_0 = util.compute_reachability_matrix(F,d,d_max)
    covered_0 = [[] for i in range(k)]
    if X_start != None:
        X = X_start
        for i in range(k):
            covered_0[i]=list(np.flatnonzero(np.any(reach_0[:,X[i],i],axis=1)))
            reach_0[:,X[i],i] = 0
            reach_0[covered_0[i],:,i] = 0
            
    
    covered = covered_0.copy()
    reach = reach_0.copy()
    C = np.sum(reach,axis=0)
    for i in range(k):
        #reset "lookahead"-part of reach matrix, coverage matrix and covered locations
        reach[:,:,i:] = reach_0[:,:,i:].copy()
        C=np.sum(reach,axis=0)
        for i_prime in range(i,k):
            covered[i_prime]=covered_0[i_prime].copy()
        
        candidates = range(m) if i==0 else X[i-1]
        #compute partial solution X_i
        indices = range(i,k)
        while np.sum(V[covered[i],2]) < pop_min:
            #find heuristic optimum 
            j_star = max(candidates,key=lambda j: tuple(C[j,indices]))
            #add facility to solution and delete from reach matrix
            X[i].append(j_star)
            for i_prime in range(i,k):
                #mark locations as covered in all remaining levels
                locations = np.flatnonzero(reach[:,j_star,i_prime])
                covered[i_prime].extend(locations)
                #delete locations from reach matrix
                reach[locations,:,i_prime]=0
            #delete facilitiy from reach matrix
            reach[:,j_star,i:]=0
            #update coverage matrix
            C=np.sum(reach,axis=0)
        
        #restore higher level coverability
        for j in range(k-1,i,-1):
            indices = list(range(j,i,-1))
            while np.sum(V[covered[j],2]) < pop_min:
                #find heuristic optimum
                j_star = max(candidates,key=lambda j: tuple(C[j,indices]))
                #add facility to solution
                X[i].append(j_star)
                for j_prime in range(j,i,-1):
                    #mark locations as covered
                    locations = np.flatnonzero(reach[:,j_star,j_prime])
                    covered[j_prime].extend(locations)
                    #update reach matrix
                    reach[:,j_star,j_prime]=0
                    reach[locations,:,j_prime]=0
                #update coverage matrix
                C=np.sum(reach,axis=0)
    return X

def balanced_cover(V,F,d,d_max,alpha,X_start = None):
    """
    special input X_start : list of lists of facility indices, solution headstart 
    returns : solution X = [X_1,...,X_k] with X_i being lists of indices of solution facilities
    """
    #initializations
    n,m = d.shape
    k = len(d_max)
    pop = np.sum(V[:,2])
    pop_min = (1-alpha)*pop
    X = [[] for i in range(k)]
    reach_0 = util.compute_reachability_matrix(F,d,d_max)
    covered_0 = [[] for i in range(k)]
    if X_start != None:
         X = X_start       
         for i in range(k):
            covered_0[i]=list(np.flatnonzero(np.any(reach_0[:,X[i],i],axis=1)))
            reach_0[:,X[i],i] = 0
            reach_0[covered_0[i],:,i] = 0
            
    
    covered = covered_0.copy()
    reach = reach_0.copy()
    C = np.sum(reach,axis=0)
    
    for i_outer in range(k):
        #reset "lookahead"-part of reach matrix, coverage matrix and covered locations
        reach[:,:,i_outer:] = reach_0[:,:,i_outer:].copy()
        C=np.sum(reach,axis=0)
        for i_prime in range(i_outer,k):
            covered[i_prime]=covered_0[i_prime].copy()

        for i in reversed(range(i_outer,k)):
            #find facility on lookahead level i that is optimal for coverage of current level i_outer
            indices = range(i_outer,k)
            candidates = np.flatnonzero(F[:,2]>=i)
            if i_outer>0:
                candidates = [j for j in candidates if j in X[i_outer-1]]
            while np.sum(V[covered[i],2]) < pop_min:
                #find heuristic optimum
                j_star = max(candidates,key=lambda j: tuple(C[j,indices]))
                #add facility to solution
                X[i_outer].append(j_star)
                for i_prime in range(i_outer,i+1):
                    #mark locations as covered
                    locations = np.flatnonzero(reach[:,j_star,i_prime])
                    covered[i_prime].extend(locations)
                    #update reach matrix
                    reach[:,j_star,i_prime]=0
                    reach[locations,:,i_prime]=0
                #update coverage matrix
                C=np.sum(reach,axis=0)
    return X

#======================== UTILITY ===============================

