"""
small script which was used for generating data for plotting examples (then plotted with R)
"""

import instance as ins
import numpy as np
import pandas as pd
import algorithms
from scripts import generate_locations


names = ["nrw"]
facility_level_restrictions = [[1,0.25]]
freedom_factors = [1]
alphas = [0.05]
d_max_vectors = {2:[[1,2]]}
instance = ins.getInstances(names,facility_level_restrictions,d_max_vectors,alphas,freedom_factors)[0]

V,F,d,d_max,alpha = instance["instance"]

path = "~/dev/facilityreduction/data/example/"

V_new = generate_locations.convert(V)

pd.DataFrame(V_new).to_csv(path+"V_samples.csv")

pd.DataFrame(V).to_csv(path+"V_0.csv",index=False)
pd.DataFrame(F).to_csv(path+"F_0.csv",index=False)

F_1,d_1 = algorithms.compute_nondominated_facilities(F,d,d_max)
V_1,d_2 = algorithms.reduce_coverage_classes(V,F_1,d_1,d_max)

pd.DataFrame(V_1).to_csv(path+"V_cover_red.csv",index=False)
pd.DataFrame(F_1).to_csv(path+"F_nondominated.csv",index=False)

V,d,alpha = algorithms.area_sweep(V,F,d,d_max,alpha)

pd.DataFrame(V).to_csv(path+"V_static.csv",index=False)


F,d = algorithms.compute_nondominated_facilities(F,d,d_max)
V,d = algorithms.compute_nondominated_locations(V,F,d,d_max)

pd.DataFrame(V).to_csv(path+"V_nondominated.csv",index=False)
