"""
some utility functions
"""

import numpy as np


def compute_distance_restrictions(V,F,d,alpha):
    """
    Computes base distance restrictions
    """
    n,m = d.shape
    k = round(np.max(F[:,2])+1)
    d_max = np.zeros(k)

    #compute V_prime
    pop = np.sum(V[:,2])
    min_distances = np.min(d,axis=1)
    order = sorted(range(n),key=lambda l: min_distances[l])
    s = np.cumsum(V[order,2])
    last_index = np.flatnonzero(s>(1-alpha)*pop)[0]
    V_prime = order[:last_index+1]

    #compute d_max
    for i in range(k):
        facilities = list(np.flatnonzero(F[:,2]>=i))
        d_max[i] = np.max(np.min(d[V_prime,:][:,facilities],axis=1))

    return d_max



def compute_reachability_matrix(F,d,d_max):
    """
    returns: reach : boolean (n,m,k)-matrix where reach[l,j,i]=True iff facility j is reachable from location l on level i 
    """
    n,m = d.shape
    k = len(d_max)
    reach = np.empty((n,m,k),bool)
    for i in range(k):
        reach[:,:,i]=d<=d_max[i]
        reach[:,[j for j in range(m) if F[j,2]<i],i]=False
    
    return reach

def is_feasible(X,V,F,d,d_max,alpha):
    """
    returns true iff the given solution is feasible
    """
    n,m = d.shape
    k = len(d_max)
    pop = np.sum(V[:,2])
    reach = compute_reachability_matrix(F,d,d_max)

    valid = True

    #check coverage on each level
    
    perc = np.zeros(k)
    for i in range(k):
        covered = np.any(reach[:,X[i],i],axis=1)
        perc[i]=np.sum(V[covered,2])/pop
    #print("percentage covered on each level: ",perc)
    if(np.any(perc<1-alpha)):
        print("insufficient coverage!")
        print("percentage covered on each level: ",perc)
        valid = False


    #check chain condition
    for i in range(k-1):
        if not set(X[i]).issuperset(set(X[i+1])):
            print("chain condition not satisfied on level ",i)
            valid = False

    #check level restrictions
    for i in range(k):
        if not np.all(F[X[i],2]>=i):
            print("facility level restrictions violated on level ",i)
            valid = False
    
    #print("solution is feasible!")
    return valid

def is_valid_instance(V,F,d,d_max,alpha):
    """
    returns true iff the given instance is valid
    """
    n,m = d.shape
    k = len(d_max)
    pop = np.sum(V[:,2])
    reach = compute_reachability_matrix(F,d,d_max)

    covered = np.all(np.any(reach,axis=1),axis=1)
    perc = np.sum(V[covered,2])/pop
    if(perc<1-alpha):
        print("insufficient coverage!")
        print("percentage covered: ",perc)
        return False
    #print("valid instance!")
    return True