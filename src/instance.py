

"""
Provides functionality concerning instances. 
An instance has the following structure:
I=[V,F,d,d_max,alpha] with
V : (n,3)-nd-array, locations (lat, long) and their population
F : (m,3)-nd-array, facilities (lat, long) and their max level (0,...,k-1)
d : (n,m)-float-ndarray, distances between locations and facilites
d_max : k-float-list, distance restrictions for each facility level
alpha : float, margin of error
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import util

def getInstances(names, facility_level_restrictions, d_max_vecs, alphas, freedom_factors):
    """
    Returns a list of instances given possible parameters.

    Parameters:
        names : list, instance names, currently only supports "nrw", "bayern", "sachsen", "bw"
        facility_level_restrictions : list of lists, relative sizes (m_1/m,...,m_k/m) of facilitiy levels
        d_max_vecs : dict of lists, factors k->(beta_1,...,beta_k) applied to (d_1=...=d_k) in the case of F_1=...=F_k
        alphas : list, margins of error
        freedom_factors : list, freedom factors beta, that will be applied to d_max

    Returns:
        instances: list of dicts containing instance information and the instance (V,F,d,d_max,alpha) itself under the key "instance"


    """

    V_dict, F_dict, d_dict = {}, {}, {}
    for name in names:
        V_dict[name] = pd.read_csv("data/final/locations_"+name+"_pop.csv").to_numpy()

        F = pd.read_csv("data/final/hospital_locations_"+name+".csv").to_numpy()
        m = F.shape[0]
        F = np.hstack([F,np.zeros(m)[:,np.newaxis]])
        F_dict[name] = F

        d_dict[name] = pd.read_csv("data/final/distances_"+name+".csv",header=None).to_numpy()

    instances = []
    for name in names:
        for levels in facility_level_restrictions:
            V = V_dict[name]
            F = F_dict[name].copy()
            d = d_dict[name]
            n,m = d.shape
            k = len(levels)

            if np.min(levels)==1:
                #if no level restrictions, use alpha to compute base reachability restriction d_0 and use factors to compute d_max from that
                F[:,2]=k-1
                for alpha in alphas:
                    d_max_0 = np.ceil(util.compute_distance_restrictions(V,F,d,alpha)[0])
                    for d_max_factors in d_max_vecs[k]:
                        d_max = d_max_0*np.array(d_max_factors)
                        for factor in freedom_factors:
                            d_max = np.ceil(factor * d_max)
                            
                            prefix = name+"_"+str(levels)+"_"+str(factor)+"_"+str(alpha)
                            instances.append({"name":name,"levels":levels,"factor":factor,"alpha":alpha,"prefix":prefix,"instance":(V.copy(),F.copy(),d.copy(),d_max.copy(),alpha)})
            else:
                #if level restrictions, generate levels and use alphas and d_max vecs
                #generate levels
                
                indices = range(m)
                for i in range(k):
                    np.random.seed(((hash(name)+hash(str(levels))) % 10000 )*(i+1))
                    indices = np.random.choice(indices,round(levels[i]*m),replace=False, )
                    F[indices,2]=i

                for factor in freedom_factors:
                    for alpha in alphas:

                        #compute distance restrictions
                        d_max = np.ceil(factor*util.compute_distance_restrictions(V,F,d,alpha))

                        #add instance
                        prefix = name+"_"+str(levels)+"_"+str(factor)+"_"+str(alpha)
                        instances.append({"name":name,"levels":levels,"factor":factor,"alpha":alpha,"prefix":prefix,"instance":(V.copy(),F.copy(),d.copy(),d_max,alpha)})

    return instances

def plotInstance(V,F,X=[]):
    """
    temporary function for quicky showing some plots
    """
    n=V.shape[0]
    m = F.shape[0]
    k = np.max(F[:,2],0)

    plt.scatter(V[:,0],V[:,1],s=0.25)

    if(len(X)>0):
        F[:,2]=0
        for i in range(len(X)):
            F[X[i],2]=i
        F=F[X[0],:]
    long = F[:,0]
    lat = F[:,1]
    lvl = F[:,2]

    sizes=5*(lvl+1)


    plt.scatter(long,lat,s=sizes**2, c = 'r')
    plt.show()


def getMaxDistances(F,d):
    """
    returns list of maximal distances on each level, unused
    """
    m = F.shape[0]
    k = round(np.max(F[:,2]))+1
    max_distances = np.zeros(k)
    print(np.amax(np.amin(d,axis=1)))
    for i in range(k):
        max_distances[i]=np.amax(np.amin(d[:,[j for j in range(m) if F[j,2]>=i]],axis=1))
    return max_distances
