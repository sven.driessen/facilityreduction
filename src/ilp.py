"""
ILP formulation
"""

import time
import gurobipy as gp
import numpy as np
from gurobipy import GRB 
import pandas as pd
import util

def model(V,F,d,d_max,alpha):
    """
    returns: md, gurobi ilp model
    """
    n,m = d.shape
    k = len(d_max)
    pop = np.sum(V[:,2])
    reach = util.compute_reachability_matrix(F,d,d_max)
    #define constants
    
    M = np.zeros((k,m))
    for i in range(k):
        M[i,:] = F[:,2]>=i

    #create gurobi model
    md = gp.Model()

    #define variables
    x = md.addVars([(i,j) for i in range(k) for j in range(m)],vtype=GRB.BINARY, name = "x")
    z = md.addVars([(i,l) for i in range(k) for l in range(n)], vtype=GRB.BINARY, name="z")
    
    #set multiple objectives
    for i in range(k):
        md.setObjectiveN(x.sum(i,'*'),i,k-i-1)
    
    #minimize
    md.setAttr("ModelSense", 1)

    #define constraints
    #x_ij<=m_ij
    md.addConstrs((x[i,j] <= M[i,j] for i,j in x.keys()))

    #x_ij-x_{i-1}j<=0 
    md.addConstrs((x[i,j]-x[i-1,j] <= 0 for i,j in x.keys() if i>=1))

    #sum_l(p_l*z_il) >= (1-alpha)*p(V)
    pop_dict = {key: 1000*V[key[1],2]/pop for key in z.keys()}
    md.addConstrs((z.prod(pop_dict,i,'*') >= (1-alpha)*1000 for i in range(k)))

    #sum_{j covering l on lvl i}(x_ij)>=z_il
    md.addConstrs((gp.quicksum(x[i,j] for j in np.flatnonzero(reach[l,:,i])) >= z[i,l] for l in range(n) for i in range(k)))

    md.update()

    return md

    
def solve_relaxation(V,F,d,d_max,alpha):
    """
    Creates ILP model and solves the LP relaxation
    returns: solution X
    """
    n,m = d.shape
    k = len(d_max)


    md = model(V,F,d,d_max,alpha)
    md = md.relax()
    md.update()

    md.optimize()
    #results
    X = []
    for i in range(k):
        X.append([md.getVarByName(f"x[{i},{j}]").X for j in range(m)])
    
    return X
    

    