"""
script for performing the evaluation
"""

import time
import algorithms, util, instance, ilp
import numpy as np
from tqdm import tqdm
import pandas as pd


def write_sol(X,filepath):
    sol_array = np.zeros((m,k))
    for i in range(k):
        sol_array[X[i],i]=1
    pd.DataFrame({f"X_{i}":sol_array[:,i] for i in range(k)}).to_csv(filepath,header=True,index=True)

EVAL_INSTANCE_OPT = False
EVAL_LP_RELAXATION = True
EVAL_SOLUTION_HEURISTICS = True

#=============== INSTANCES ================= 
print("=== setting up instances ===") 
names = ["nrw","bayern","sachsen","bw"]
facility_level_restrictions = [[1],[1,1],[1,0.25],[1,1,1,1,1],[1,0.8,0.6,0.4,0.2]]
freedom_factors = [1,1.5]
alphas = [0.05]
d_max_vectors = {1:[[1]],2:[[1,2]],5:[[1,1.25,1.5,1.75,2]]}
instances = instance.getInstances(names,facility_level_restrictions,d_max_vectors,alphas,freedom_factors)
for I in instances:
    V,F,d,d_max,alpha = I["instance"]
    if not util.is_valid_instance(V,F,d,d_max,alpha):
        raise Exception("instance not valid")
   
rows = []
print("=== begin evaluation ===")

for i in tqdm(range(len(instances))):
    I = instances[i]
    #instance entries
    eval = {}
    V,F,d,d_max,alpha = I["instance"]
    n,m = d.shape
    k = len(d_max)
    pop = np.sum(V[:,2])

    eval["instance_id"] = i+1
    eval["name"] = I["name"]
    eval["n"] = n
    eval["m"] = m
    eval["k"] = k
    for i in range(k):
        eval["m_"+str(i+1)] = round(I["levels"][i]*m)
        eval["d_"+str(i+1)] = d_max[i]
    eval["alpha"] = alpha

    prefix = I["prefix"]
    

    if EVAL_LP_RELAXATION:

        #===== LP relaxation =====
        X = ilp.solve_relaxation(V,F,d,d_max,alpha)
        for i in range(k):
            eval["x_lp_"+str(i+1)] = np.sum(X[i])

    # ==================== SOLUTION HEURISTICS =============================
    if EVAL_SOLUTION_HEURISTICS:
        algorithm_id = 1
        #===== location reduction =====
        if alpha == 0:
            location_reduction = ["none"]
        else:
            location_reduction = ["anchor_deletion","area_sweep","none"]
        
        for lr in location_reduction:
            eval["location_reduction"] = lr

            t_0 = time.time()
            if lr == "anchor_deletion":
                V_0, d_0, alpha_0 = algorithms.delete_anchors(V,F,d,d_max,alpha)
            elif lr == "area_sweep":
                V_0, d_0, alpha_0 = algorithms.area_sweep(V,F,d,d_max,alpha)
            else:
                V_0, d_0, alpha_0 = V, d, alpha

            if not util.is_valid_instance(V_0,F,d_0,d_max,alpha_0):
                        raise Exception("instance not valid after location reduction!")
             
            delta_t = time.time()-t_0
            eval["n_reduced_locations"] = V_0.shape[0]
            eval["delta_t_location_reduction"] = delta_t
            
            #===== instance optimization =====
            #nondominated facilities
            t_0 = time.time()
            F_1, d_1 = algorithms.compute_nondominated_facilities(F,d_0,d_max)
            delta_t = time.time()-t_0
            eval["m_opt"] = F_1.shape[0]
            eval["delta_t_instanceopt_facilities"] = delta_t

            #locations
            if alpha_0 == 0:
                instance_opts = ["reduced", "full"]
            else:
                instance_opts = ["reduced"]
            
            for io in instance_opts:
                eval["instance_optimization"] = io
                t_0 = time.time()
                if(io == "reduced"):
                    V_2, d_2 = algorithms.reduce_coverage_classes(V_0,F_1,d_1,d_max)
                elif(io == "full"):
                    V_2, d_2 = algorithms.compute_nondominated_locations(V_0,F_1,d_1,d_max)

                delta_t = time.time()-t_0

                eval["n_opt"] = V_2.shape[0]
                eval["delta_t_instanceopt_locations"] = delta_t
                #===== solution heuristics =====
                if alpha_0==0:
                    essential_facilities = ["essentials","no_essentials"]
                else:
                    essential_facilities = ["no_essentials"]

                for ef in essential_facilities:
                    X_start = None
                    eval["essential_facilities"] = ef
                    if ef == "essentials":
                        t_0 = time.time()
                        X_start = algorithms.get_essential_facilities(F_1,d_2,d_max)
                        delta_t = time.time()-t_0
                        eval["num_essential_facilities"] = len(X_start[0])
                        eval["delta_t_essentials"] = delta_t
                    

                
                    solution_heuristics = ["top-down","bottom-up","balanced"]
                    for sh in solution_heuristics:
                        eval["solution_heuristic"] = sh
                        

                        if not util.is_valid_instance(V_2,F_1,d_2,d_max,alpha_0):
                            raise Exception("instance not valid after location reduction and optimization!")

                        t_0 = time.time()
                        if sh == "top-down":
                            X = algorithms.top_down_cover(V_2,F_1,d_2,d_max,alpha_0,X_start)
                        elif sh == "bottom-up":
                            X = algorithms.bottom_up_cover(V_2,F_1,d_2,d_max,alpha_0,X_start)
                        else:
                            X = algorithms.balanced_cover(V_2,F_1,d_2,d_max,alpha_0,X_start)
                        delta_t = time.time()-t_0

                        if not util.is_feasible(X,V_2,F_1,d_2,d_max,alpha_0):
                            raise Exception("solution not feasible!")
                        
                        eval["delta_t_solution_heuristic"] = delta_t
                        for i in range(k):
                            eval["x_"+str(i+1)] = len(X[i])
                        
                        eval["algorithm_id"]=algorithm_id
                        write_sol(X,"eval/solutions/"+prefix+"_"+str(algorithm_id)+"_"+lr+"_"+io+"_"+ef+"_"+sh+".csv")
                        algorithm_id+=1
                        
                        #add row to table
                        rows.append(eval.copy())


pd.DataFrame(rows).to_csv("eval/solution_heuristics.csv",header=True)
                    

                
                








