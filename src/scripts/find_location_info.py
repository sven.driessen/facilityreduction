"""
Script used to find the state (Bundesland) that each of the cells in the population grid belongs to.

This subdivides the grid into different states, allowing the construction of multiple smaller instances instead 
of one large one (i.e. the whole country, which proved too large to work with for now.)

Uses a local Nominatim server because the public one allows only limited requests per second.
Used map data: http://download.geofabrik.de/europe/germany.html

"""
from geopy.geocoders import Nominatim
import pandas as pd
import numpy as np
from tqdm import tqdm 

#connect to local Nominatim server
locator = Nominatim(domain='127.0.0.1:8080',scheme='http',timeout=3)

#setup grid coordinates
df = pd.read_csv("data/converted_population_1km_cleaned.csv")
coords = [(row[1],row[0]) for row in df.to_numpy()]

#get the state (Bundesland) of every grid cell
states = []
for coord in tqdm(coords):
    try:
        location = locator.reverse(coord)
        states.append(location.raw['address']['state'])
    except:
        states.append(None)

#export the results
df["state"]=states
df.to_csv("data/converted_population_1km_cleaned_extended.csv",index=False)