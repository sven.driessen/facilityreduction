"""
Script used to calculate the distances between each location-hospital-pair of the NRW-data.

Uses a local OSRM server with current map data from NRW 
(http://download.geofabrik.de/europe/germany/nordrhein-westfalen.html)
to calculate the distances.
Did not yet work for the whole of germany because of the high computation time. Might work in the future with
some optimizations.
"""
import pandas as pd
import numpy as np
import osrm
from tqdm import tqdm

#DOCKER SETUP: (change map name accordingly)
#sudo docker run -t -v "$PWD:/data" osrm/osrm-backend osrm-extract -p /opt/car.lua /data/baden-wuerttemberg-latest.osm.pbf
#sudo docker run -t -v "$PWD:/data" osrm/osrm-backend osrm-contract /data/baden-wuerttemberg-latest.osrm
#sudo docker run -t -i -p 5000:5000 -v "$PWD:/data" osrm/osrm-backend osrm-routed --algorithm ch --max-table-size 100000 /data/baden-wuerttemberg-latest.osrm

#prepare location data
locations = pd.read_csv("data/final/locations_bw_pop.csv").to_numpy()
locations = [(row[0],row[1]) for row in locations]

#prepare hospital data
hospitals = pd.read_csv("data/final/hospital_locations_bw.csv").to_numpy()
hospitals = [(row[0],row[1]) for row in hospitals]

#get distance table using osrm
distances = osrm.table(coords_src=locations,coords_dest=hospitals)[0]
#convert to minutes
distances/= 60.
distances = np.round(distances,2)

pd.DataFrame(distances).to_csv("data/final/distances_bw.csv",header=False, index=False)