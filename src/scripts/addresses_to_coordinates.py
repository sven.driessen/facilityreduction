"""
Script used to convert the descriptive adresses of the original hospital location list into
GCS coordinates.

Uses a public remote Nominatim server to geocode the locations.

"""

import pandas as pd
from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter
from tqdm import tqdm

#read address data
with open("data/KHV18.csv","r",encoding="utf-8") as f:
    data = f.readlines()
    for i in range(len(data)):
        data[i]=data[i].rstrip("\n") + ", Germany"

#create new data frame with address column
df = pd.DataFrame()
df["address"] = data

#compute longitude and latitude for each address
geolocator = Nominatim(user_agent="svend99@web.de")
geocode = RateLimiter(geolocator.geocode,min_delay_seconds=1)
tqdm.pandas()
locations = df["address"].progress_apply(geocode)
longitude, latitude = [], []
for l in locations:
    if l is not None:
        latitude.append(l.latitude)
        longitude.append(l.longitude)
    else:
        latitude.append(None)
        longitude.append(None)

#add latitude and longitude columns to data frame
df["latitude"] = latitude
df["longitude"] = longitude

#write the data frame into a csv file
df.to_csv("data/hospital_locations_2.csv")

