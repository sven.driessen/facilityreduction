"""
Script used to convert the coordinates of the population-grid cell center points from ETRS89 to GCS coordinates
"""
from pyproj import Proj, transform
import pandas as pd
from tqdm import tqdm

inProj = Proj(init='epsg:3035')
outProj = Proj(init='epsg:4326')

def convert(row):
    x,y = transform(inProj,outProj,row[0],row[1])
    return pd.Series([x,y,row[2]])

df = pd.read_csv("data/population_1km.csv")
tqdm.pandas()
columns = df.columns
df = df.progress_apply(convert,axis=1)
df.columns = columns
df.to_csv("data/converted_population_1km_2.csv",index=False)