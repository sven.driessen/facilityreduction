"""
Script used to generate a set of locations from grid-cell population data with cells of 1km sidelength.
Per cell, randomly (uniformly) samples one location in each populated cell, and samples 1 more location per 500 population.
This approach serves to estimate the population density in the given region.
"""
import pandas as pd
import numpy as np
from tqdm import tqdm

#population per location
popPerLoc = 500
#which state to generate locations for
state = "Nordrhein-Westfalen"
def boundingBox1km(long, lat):
    """
    Function that returns the boundaries of a 1km bounding box in GCS coordinates
    Formulas taken from https://en.wikipedia.org/wiki/Geographic_coordinate_system#Length_of_a_degree
    """
    phi = lat*2*np.pi/360
    dLat = 1000/(111132.92-559.82*np.cos(2*phi)+1.175*np.cos(4*phi)-0.0023*np.cos(6*phi))
    dLong = 1000/(111412.84*np.cos(phi)-93.5*np.cos(3*phi)+0.118*np.cos(5*phi))

    latMin,latMax,longMin,longMax = lat-dLat/2, lat+dLat/2, long-dLong/2, long+dLong/2
    return longMin, longMax, latMin, latMax

def generateLocations(row):
    """function that generates the locations for one grid cell"""
    if(row[2]<1): return []
    longMin, longMax, latMin, latMax = boundingBox1km(row[0],row[1])
    #one base location + 1 extra location per 500 population in the current cell
    numLocations = int(np.ceil(row[2]/popPerLoc))
    locations = np.random.rand(numLocations,2)*np.array([longMax-longMin,latMax-latMin])[np.newaxis,:] + np.array([longMin,latMin])[np.newaxis,:]
    return locations

def convert(V):
    """function that takes list of grid cells and returns list of locations generated from all grid cells"""
    location_list = []
    for row in tqdm(V):
        location_list.extend(generateLocations(row))
    locations = np.vstack(location_list)
    return locations

cells = pd.read_csv("data/tmp/converted_population_1km_cleaned_extended.csv")
#filter by state
cells = cells[cells["state"]==state].to_numpy()

#generate locations

locations = convert(cells)

df = pd.DataFrame(locations,columns=["longitude","latitude"])
df.to_csv("data/final/locations_"+state+"_"+str(popPerLoc)+".csv",index=False)


