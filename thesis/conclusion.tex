\section{Conclusion and Outlook}
In this thesis, we modeled the problem of minimizing the number of facilities required to satisfy reachability restrictions on multiple levels, 
which originated in the problem of overcapacieties in the hospital infrastructure in germany mainly due to an outdated sector-based structure, 
as a combinatorial optimization problem.\\
We examined the special case where the instances have the structure of a tree with a few additional 
restrictions and were able to develop a polynomial-time algorithm for this problem. \\
We then proved that the general problem is not solvable 
in polynomial time, even if we restrict it to metric instances. \\
We formulated the problem as an integer program including an alternative 
version with a single objective in addition to the original multi-objective variant. \\
We then developed multiple methods for instance optimization, 
including the eliminiation of dominated facilities, which is always possible, and two variant for optimizing locations: eliminating dominated
facilities, which yields in a higher level of optimization but requires harsher restrictions, and coverage reduction, which is always possible 
to perform but yields a lower level of optimization. \\
We then introduced the notions of static and dynamic instances, where static instances 
require all locations to be covered while dynamic instances allow for a margin of error. We introduced the concept of a static reduction, which
turns a dynamic instance into a static one by pre-computing a fixed location set, thereby allowing for better optimization and the use 
of essential facilities, but at the same time restricting the freedom of the solution and thereby possibly reducing the optimal solution value.
We introduced two heuristics for static reduction, including anchor deletion, which eliminates locations that provide the harshest restrictions to the
range of solutions, and area sweep, which prioritizes eliminating as many locations as possible.\\
We further introduced the notion of essential facilities, which exist in static instances, are easily identified and are guaranteed to be contained 
in any solution.\\
We developed three final solution heuristics, which are all based on a greedy heuristic for the Set Cover problem which is known to have an optimal
approximation ratio that depends on the size of the covers. The heuristics balance the two competing goals of achieving optimal coverage on the current level 
and similtaniously maintaining higher level coverability. The top-down algorithm focuses on higher level coverability first by simply starting
with the partial solution for the highest level and working downwards. The bottom-up algorithm does the opposite, focusing on optimal coverage by 
starting at the first level, working upwards and only maintaining higher level coverability after a partial solution of the current level has 
already been found. The balanced algorithm balances both approaches by computing the partial solutions in a bottom-up fashion, but selecting the 
corresponing facilities in a top-down fashion. \\
Finally, we implemented and evaluated all algorithms on a set of instances defined by a multitude of parameters corresponding to important characteristics.
In this evaluation we learned that using a static reduction and the better optimization that are thereby made possible does result in much smaller, more 
optimized instances, but negatively affects the solution quality and requires a high pre-processing time. We learned that Anchor Deletion is the superior 
heuristic to Area Sweep in all regards, that precomputing essentia facilities is very efficient and effective, and that all three solution heuristics 
result in a similar solution quality, but with the top-down heuristic having a superior running time.\\

There are certainly questions that are left unanswered in this thesis that might very well be worth pursuing further. \\
For one, it might be interesting 
to see how the properties and the complexity of the problem on trees changes if one or both of the additional restrictions $F_1=...=F_k$ and $\alpha=0$ 
are removed.\\
We have seen that the running time of the instance optimizations, especially the elimination of dominated facilities, poses a problem if the time 
available for instance pre-processing is limited. While the current implementation is very naively following the definition, it may be possible to
use specialized data structures to signinficantly improve its performance. \\
The solutions we obtained using our greedy heuristics should be used as an initial instance for a subsequent \emph{local search}, which could greatly
improve the solution quality and which would additionally benefit from the advanced instance optimizations, as a local search is generally very time-consuming,
and the solution quality would also benefit from the resulting increase of iteration speed.\\

Lastly, there are of course many aspects of the underlying healthcare problem setting that could also be incorporated into the problem model. For example,
one could include further requirements for the facilities, like minimum and maximum case numbers. One could also further distinguish the facilities and their
offered services from one another, for example by including hospitals with different specializations, resulting in a much more detailed structure than 
the relatively simple hierarchical level-structure used in our model.\\

The optimization of healthcare infrastructure is a very important, but challenging field. With methods of combinatorial optimization, we have the possibility 
to rigorously and systematically model a wide range of its problems, and use well-researched and optimized methods for finding provably good solutions.
After over a century of research in facility location problems, new challenges still remain, and multi-level location planning under reachability restrictions
is no exception. This thesis provided a starting point for this problem, with first insights about its complexity, special cases, optimizations and 
heuristics, but there is still much to be explored and improved upon. Further research in this area could have a significant impact on improving the 
efficiency, accessibility, and quality of healthcare systems, ultimately benefiting society as a whole.

