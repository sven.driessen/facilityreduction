\section{Mathematical Model} 
\label{sec:model}

We will now derive a generalized mathematical model of the problem that we will then use as a basis for the rest of the thesis. \\
We model the setting of the problem by a set $V$ of vertices representing a discretization of possible locations that should be covered 
by the facilities. Optionally, each location $v\in V$ may be assigned a \emph{population label} $p(v)$, representing the size of the 
population of the area which is represented by the location $v$.
The facilities are given as a set $F$. For every facility $f\in F$, we assign a 
distance label $d(f,v)\geq 0$ to every vertex $v\in V$, representing the distance between $f$ and $v$.
(Note: The set $V$ might be the vertex set of a graph representing a street network. In this case, the distances are then obtained 
by shortest paths in the graph. But since we do not care about the actual graph structure and we want to keep our model as 
general as possible, we choose the minimalist approach described above.)\\
Instead of only considering basic and maximum hospital care, we generalize our model to include an arbitrary finite number 
of \emph{facility levels}. Since not every facility is able to offer every level of services, we include a maximum facility level 
for every facility. We model this by a descending chain of facility subsets $F=F_1\supseteq ...\supseteq F_k$, 
each subset $F_i$ representing one facility level $i$. Then for each facility $f$ we have $f\in F_i$ if and only if the maximum 
facility level offered by $f$ is level $i$. \\
We model the reachability requirements by including a maximum distance $d_i\geq 0$ for each facility-level, 
representing the maximal distance from any location to the nearest facility of level $i$. \\

Based on this setting we define a feasible solution to be a descending chain $F\supseteq X_1\supseteq ...\supseteq X_k$ of facility-subsets 
such that for all $i\in\{1,...,k\}$, $X_i\subseteq F_i$ and $X_i$ satisfies the corresponding reachability requirement for up to an allowed 
margin of error $\alpha$ (this will soon be defined more precisely in Definition~\ref{def:problem}). 
This defines the set $\mathcal{X}$ of feasible solutions. \\
In the original problem in the Bertelsmann survey, we first find a \emph{fixed} minimal set $X_1$ of hospitals 
that should offer basic hospital care, and then proceed to find a minimal set $X_2\subseteq X_1$ of hospitals that should offer 
maximum hospital care in this already fixed set. A problem with this approach is that it only ensures local optimality after 
fixing $X_1$, since $X_1$ is not unique: there might be another solution $X_1'\supseteq X_2'$ with $|X_1'|=|X_1|$ and $|X_2'|<|X_2|$ that 
we would not have found with this approach. \\ %TODO: example
To solve this problem, in our model we will take all sets $X_i$ with optimal cardinality into account and thus ensure global optimality 
of an optimal solution. However, this approach does not allow us to split our problem up into multiple sub-problems as in the Bertelsmann survey. 
Instead, we need to optimize over the whole multi-dimensional search space $\mathcal{X}$. 
To define what an optimal solution in this search space is, we equip it with an appropiate total order:
\begin{shaded}
\begin{definition}[lexicographical order] \hfill \\
Let $(X,<)$ be a totally ordered set and $d\in\N$. 
Then we obtain a total order $<_{lex}$ on the space $X^d$ defined by
\[(x_1,...,x_d)<_{lex}(y_1,...,y_d) \Leftrightarrow \exists k: x_i=y_i \text{ for all } i<k \text{ and } x_k<y_k,\]
called the \emph{lexicographical order} on $X^d$.
\end{definition}
\end{shaded}
We now have all the ingredients to properly define our optimization problem:
\begin{shaded}
\begin{definition}[Constrained Facility Reduction Problem (\textsc{CFR})] \label{def:problem} \hfill \\
\textbf{Given:}    
     A nonempty set of vertices $V$ (also called \emph{locations}) with population labels $p:V\rightarrow\N$, a set of facilities $F$, 
     distances $d(f,v)=d(v,f)\geq 0$ for each facility $f\in F$ and each vertex $v\in V$,
      an assignment $F\rightarrow\{1,...,k\}$ of facilities to $k$ facility levels given by subsets $F=F_1\supseteq...\supseteq F_k$,
     distance/reachability restrictions $d_i\geq 0$ for each facility level $i\in\{1,...,k\}$,
     and an allowed margin of error $\alpha\in[0,1]$, such that there exists a subset $V'\subseteq V$ 
     with $\sum_{v\in V'}p(v)=:p(V')\geq (1-\alpha)p(V)$ satisfying
     \[\bigcup_{f\in F_i}B_{d_i}(f)=V'\]
     for all $i\in\{1,...,k\}$. Here, $B_{d_i}(f):=\{v\in V|d(f,v)\leq d_i\}$ is the closed ball induced by $d$ around $f\in F$, 
     which we also call the \emph{cover} of $f$ on level $i$. 
     \\
\textbf{Find:}
Reduced facility sets $X_1,...,X_k\subseteq F$ satisfying the following properties:
\begin{itemize}
    \item[(1)] $X_i\subseteq X_{i-1}\cap F_i$ for all $i\in\{2,...,k\}$
    \item[(2)] For all $i\in\{1,...,k\}$, there exists a subset $V_i'\subseteq V$ with $p(V_i')\geq (1-\alpha)p(V)$ 
    satisfying 
    \[\max_{v\in V_i'}\min_{f\in F_i}d(v,f)\leq d_i\] 
    or equivalently
    \[V_i' \subseteq \bigcup_{f\in X_i}B_{d_i}(f)\]
    \item[(3)] $(|X_1|,...,|X_k|)$ is lexicographically minimal.
\end{itemize}
\end{definition}
\end{shaded}

We denote the set of feasible solutions (i.e. $X_1,...,X_k\subseteq F$ satisfying (1) and (2)) by $\mathcal{X}$. \\
For simplicity we may say that $(X_1,...,X_k)$ is minimal, implicitly referring to the minimality of $(|X_1|,...,|X_k|).$\\
Note that while we allow a solution to cover different sets of locations $V_i'$ on each level $i$, from instances we require the 
stronger property that there exists a \emph{fixed} set $V'$ of locations that is covered on \emph{all} levels. This requirement 
comes with a big advantage, namely that it allows the reduction to an instance with $\alpha=0$, which has many advantages. This will 
be discussed in Section~\ref{sec:algorithms}. Note that the stronger requirement can always be achieved by adjusting $\alpha$ accordingly.\\
The population labels $p$ are entirely optional. Note that each instance $I=(V,F,d,p,k,(F_1,...,F_k),(d_1,...,d_k),\alpha)$
\emph{with} population labels can be transformed into an equivalent instance $I'$ \emph{without} population labels by including 
$p(v)$ copies of each vertex $v\in V$. An instance \emph{without} population labels can be interpreted as an instance where every location 
$v$ has a population of $p(v)=1$, in which case $p(U)=|U|$ for $U\subseteq V$.
Using population labels becomes necessary when we want to ''condense`` locations to
reduce the size of the instance, as we will see in section~\ref{sec:instanceopt}. \\
If $v\in B_{d_i}(f)$, we also say that $v$ in \emph{covered} by $f$. In this sense, $\textsc{CFR}$ can be interpreted as a 
\emph{covering problem}, in
addition to being a \emph{facility location problem}. We will see an overview of these two problem classes in the next section.

\subsection{Mathematical Background}
The Constrained Facility Reduction problem lies at the intersection of the class of \emph{facility location} problems and the class of 
\emph{covering} problems.
\paragraph*{Facility Location Problems.}
Facility location problems are concerned with the optimal placement of one or more \emph{facilities}, maximizing or minimizing some
objective function while satisfying a set of constraints. In the most common form of the problem, the goal is to find the optimal placement
of a fixed set of facilities such that some form of transportation cost between a set of customers and their nearest facility is minimized.
Additional restrictions may include fixed customer demands that have to be satisfied or maximum facility capacities. Often, the objective 
function comes in the form of a \emph{distance} function that is to be minimized. This distance function can vary in form - in some cases
we are only interested in the maximal distance between a customer and its nearest facility, as in the \emph{$K$-Center} problem.
In other cases, like in the \emph{$K$-Median} problem, we want to minimize the \emph{sum} of the distance between each customer and its nearest
facility.\\
One of the earliest and most famous facility location problems is the \emph{Weber Problem} (as it is today called), first formulated by 
german economist Alfred Weber in 1909 in \cite{weber1909standort}, which aims to find the location of a single facility $f\in\R^2$
that minimizes the sum of the weighed distances to a fixed set of points $x_i\in \R^2$ with weights $w_i\in\R_{\geq 0}$. \\
In another category of facility location problems - which our Constrained Facility Reduction problem falls under - the goal 
is to minimize the number of facilities required to satisfy given demands. These problems can also be interpreted as special cases of 
the more general class of \emph{covering problems}.



\paragraph*{Covering Problems.}
Covering problems are generally special cases of the \emph{Set Cover} problem, which aims to find a minimal selection from a a given set of
subsets (called \emph{covering sets}) of a universe of elements that cover the whole universe. The universe and the covering sets might be 
induced by any mathematical structure. For example, the universe might consist of the vertices of an undirected graph, and the 
covering sets might be the edges, which would results in the \emph{Edge Cover} problem. 
Conversely, the universe might be the set of edges of a graph, and the covering sets might be the adjacent edges of 
each vertex, resulting in the \emph{Vertex Cover} problem. On the other hand, the universe might consist of points in any metric space, and
the covering sets might be the open or closed balls induced by a set of center points. This is the category that many facility location problems
minimizing the number of facilites fall under, including \textsc{CFR} - at least the special case where $k=1$ and $\alpha=0$. 
The Set Cover problem is one of the 21 problems that Richard Karp famously proved to be NP-complete in 1972 in \cite{karp1972reducibility}. As 
such, most covering problems can be expected to be hard to solve, in the sense that there does not exist an algorithm solving them in polynomial time
under the assumption that $P\neq NP$.\\

Over the last century, facility location problems were heavily researched, and there exist a multitude of papers published on a variety of both general and very 
specialized problems.
Ahmadi-Javid, Seyedi and Syam (2017) give an extensive overview over the field of facility location problems related to healthcare in \cite{ahmadi2017survey}, including
covering-based problems. They classify a variety of healthcare facility location problems and survey different approaches for modeling and solving these problems.
Farahani, SteadieSeifi and Asgari (2010) provide an overview over multi-objective facility location problems in \cite{farahani2010multiple}. They summarize common objectives, different types of 
relations between objectives, and they describe the problems that typically arise when having multiple adversary objectives as well as the most common methods of solving 
these problems. Farahani et al. (2012) classify and review a large number of papers related to covering problems in facility location in \cite{farahani2012covering}, 
providing a great overview over the intersection of both problem classes.\\

Now that we introduced a mathematical model for our problem and explored its roots in facility location- and especially covering problems, 
we will begin our study of it by looking at an interesting special case in which the underlying structure is induced by a tree.





