\begin{thebibliography}{10}

\bibitem{ahmadi2017survey}
A.~Ahmadi-Javid, P.~Seyedi, and S.~S. Syam.
\newblock A survey of healthcare facility location.
\newblock {\em Computers \& Operations Research}, 79:223--263, 2017.

\bibitem{augurzky2014krankenhausplanung}
B.~Augurzky, A.~Beivers, N.~Straub, and C.~Veltkamp.
\newblock {\em Krankenhausplanung 2.0: Endbericht zum Forschungsvorhaben des
  Verbandes der Ersatzkassen e. V.(vdek)}.
\newblock Number~84. RWI Materialien, 2014.

\bibitem{busse2018planerischen}
R.~Busse and E.~Berger.
\newblock Vom planerischen bestandsschutz zum bedarfsorientierten
  krankenhausangebot.
\newblock {\em Krankenhaus-Report}, pages 149--170, 2018.

\bibitem{chvatal1979greedy}
V.~Chvatal.
\newblock A greedy heuristic for the set-covering problem.
\newblock {\em Mathematics of operations research}, 4(3):233--235, 1979.

\bibitem{dinur2014analytical}
I.~Dinur and D.~Steurer.
\newblock Analytical approach to parallel repetition.
\newblock In {\em Proceedings of the forty-sixth annual ACM symposium on Theory
  of computing}, pages 624--633, 2014.

\bibitem{farahani2012covering}
R.~Z. Farahani, N.~Asgari, N.~Heidari, M.~Hosseininia, and M.~Goh.
\newblock Covering problems in facility location: A review.
\newblock {\em Computers \& Industrial Engineering}, 62(1):368--407, 2012.

\bibitem{farahani2010multiple}
R.~Z. Farahani, M.~SteadieSeifi, and N.~Asgari.
\newblock Multiple criteria facility location problems: A survey.
\newblock {\em Applied mathematical modelling}, 34(7):1689--1709, 2010.

\bibitem{DLM}
{Federal Agency for Cartography and Geodesy}.
\newblock Digitales landschaftsmodell 1:250 000 (kompakt) (dlm250).
\newblock
  \url{https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/digitale-landschaftsmodelle/digitales-landschaftsmodell-1-250-000-kompakt-dlm250-kompakt.html}.
\newblock Accessed: 2022-12-13.

\bibitem{karp1972reducibility}
R.~M. Karp.
\newblock Reducibility among combinatorial problems.
\newblock In {\em Complexity of Computer Computations}, pages 85--103.
  Springer, 1972.

\bibitem{kretzler2020deutschland}
M.~Kretzler, E.~Berger, C.~Reichebner, M.~Offermanns, R.~Heber, C.~Krause,
  M.~Schulz, and R.~Busse.
\newblock Deutschland und d{\"a}nemark--verschiedene welten? ein umfassender
  vergleich der gesundheitssysteme.
\newblock {\em Gesundheits-und Sozialpolitik (G\&S)}, 74(4-5):13--20, 2020.

\bibitem{BS}
{S. Loos, M. Albrecht, K. Zicht}.
\newblock {Zukunftsfähige Krankenhausversorgung}.
\newblock Technical report, Bertelsmann Stiftung, 2019.

\bibitem{schmidt2014krankenhaus}
C.~M. Schmidt, B.~Augurzky, S.~Krolop, C.~Hentschker, and A.~Pilny.
\newblock {\em Krankenhaus Rating Report 2014: Mangelware Kapital: Wege aus der
  Investitionsfalle}.
\newblock medhochzwei Verlag, 2014.

\bibitem{Hospitals}
{Statistical Offices Germany}.
\newblock Krankenhausverzeichnis 2018.
\newblock
  \url{https://www.statistischebibliothek.de/mir/receive/DEHeft_mods_00143585}.
\newblock Accessed: 2022-12-13.

\bibitem{Zensus}
{Statistical Offices Germany}.
\newblock Zensus 2011.
\newblock
  \url{https://www.zensus2011.de/DE/Home/Aktuelles/DemografischeGrunddaten.html?nn=559100}.
\newblock Accessed: 2022-12-9.

\bibitem{sundmacher2015hospitalisations}
L.~Sundmacher, D.~Fischbach, W.~Schuettig, C.~Naumann, U.~Augustin, and
  C.~Faisst.
\newblock Which hospitalisations are ambulatory care-sensitive, to what degree,
  and how could the rates be reduced? results of a group consensus study in
  germany.
\newblock {\em Health policy}, 119(11):1415--1423, 2015.

\bibitem{weber1909standort}
A.~Weber and G.~Pick.
\newblock {\em Standort der Industrien: erster Teil: reine Theorie des
  Standorts}.
\newblock Verlag JCB Mohr, 1909.

\end{thebibliography}
