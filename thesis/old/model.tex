\section{Mathematical Models}
\subsection{Initial Model}
The first model of the problem is a direct translation into the language of graphs.
First we need to introduce some notation. \\
\begin{shaded}
\begin{notation}
Let $G=(V,A)$ be a directed graph with arc costs $c:A\rightarrow \R$. For $u,v\in V$, we define
    \[d(u,v) := \begin{cases} l(p),& \text{if there exists a shortest path } p \text{ from } u \text{ to } v \\
                            \infty,& \text{ if } u \text{ and } v \text{ are disconnected in }G
                \end{cases}
    \]
For $v\in V$ and a subset $W\subseteq V$, we define \[d(v,W):=\min_{w\in W}d(v,w)\]
\end{notation}
\end{shaded}
Now we can define the problem.
\begin{shaded}
\begin{definition}[Constrained Facility Reduction Problem (\textsc{CFR})] \hfill \\
    \textbf{Given:} A directed Graph $G=(V,A)$, arc costs $c:A\rightarrow \R$, a set of facilities $F\subseteq V$, reachability constraints $d_1\leq...\leq d_k \ (k\in \N)$.\\
    \textbf{Find:} A sequence $X=(X_1\supseteq...\supseteq X_k)$ of subsets $X_i\subseteq F$ such that for all $i\in\{1,...,k\}$, $X_i$ is a \emph{minimal} subset $X_i\subseteq X_{i-1}$ (with $X_0:=F$) satisfying $\max_{v\in V}(d(v,X_i))\leq d_i$.
\end{definition}
\end{shaded}
\textbf{Notes:}\begin{itemize}
\item We denote the search space by $\X$.
\item The indices $i\in\{1,...,k\}$ are called the \emph{facility levels}.
\item This problem can be seen as a multiobjective minimization problem with $k$ objective functions $c_1,...,c_k$ defined by $c_i(X)=|X_i|$. Multiobjective minimization problems are generally defined by non-dominating sets, and are very hard to solve. The kind of minimization used for this problem is different and much easier, since it allows for treating each objective function independently one after the other, as we will see soon. But on the other hand, this means that we treat the objective functions unequally, effectively prioritizing lower facility-levels. More precisely, the kind of minimization in this problem corresponds to a minimization of the objective function $c:\X\rightarrow \N^k, X\mapsto (|X_1|,...,|X_k|)$ using the lexicographical (total) order on $\N^k$. While this minimization conforms to the original problem definition in the Bertelsmann survey and it is easier to handle, studying the same problem as a multiobjective minimization problem using non-dominating sets is definitely an approach that might lead to very interesting results.
\end{itemize}
\subsection{Reduction to Bipartite Cut Cover}
While the first model is very intuitive, it can be reduced to a much simpler problem that is easier to handle.
\begin{shaded}
\begin{definition}[\textsc{Co-Dominating Set} Problem]  \hfill \\
    \textbf{Given:} A \emph{bipartite} Graph $G=(V=U\cup W,E)$.\\
    \textbf{Find:} A \emph{minimal} subset $X\subseteq U$ covering $W$, i.e. $N(X)=W$.
\end{definition}
\end{shaded}
\textbf{Note:} For a graph $G=(V,E)$, we denote the \emph{Neighbourhood} of a set $X\subseteq V$ by $N(X):=\{v\in V| \exists x\in X: \{x,v\}\in E\}\subseteq V$ \\

The first step is to reduce the problem to the special case of only one facility-level, i.e. the case $k=1$.
\begin{shaded}
\begin{lemma}
A \textsc{CFR}-instance with $k$ facility levels can be solved by iteratively solving $k$ \textsc{CFR}-instances with $1$ facility level.
\end{lemma}
\end{shaded}
\begin{proof}
Let $\I=(G=(V,A),F,c,(d_1,...,d_k))$ be a \textsc{CFR}-instance with $k$ facility levels. Solving the instance $(G,F,c,d_1)$ with only 1 facility level yields a minimal subset $X_1\subseteq F$ with $\max_{v\in V}(d(v,X_1))\leq d_1$. Then $\I':=(G,X_1,c,(d_2,...,d_k))$ is an instance with $k-1$ facility levels. Repeating the above process inductively yields $X_2,...,X_k\subseteq F$ satisfying $X_i\subseteq X_{i-1}$ and $\max_{v\in V}(d(v,X_i))\leq d_i$ for $i\in\{2,...,k\}$. Then $X:=(X_1,...,X_k)$ is a solution to $\I$.
\end{proof}
As a result of this lemma, we can now assume that we have only one facility level, so we only need to find one minimal subset $X\subseteq F$ with $\max_{v\in V}d(v,X)\leq d$ for a given $d\in\R$. This version of \textsc{CFR} will from now on be called the \emph{Singular Contrained Facility Reduction Problem} (\textsc{SCFR}):
\begin{shaded}
\begin{definition}[Singular Constrained Facility Reduction Problem (\textsc{SCFR})] \hfill \\
    \textbf{Given:} A directed Graph $G=(V,A)$, arc costs $c:A\rightarrow \R$, a set of facilities $F\subseteq V$, a reachability constraint $d\in\R.$\\
    \textbf{Find:} A minimal subset $X\subseteq F$ with $\max_{v\in V}d(v,X)\leq d$.
\end{definition}
\end{shaded}

In the next step, we show that \textsc{SCFR} can be reduced to \textsc{Co-Dominating Set}.
\begin{shaded}
\begin{lemma}
\textsc{SCFR} $\leq_p$ \textsc{Co-Dominating Set}
\end{lemma}
\end{shaded}
\begin{proof}
Let $\I_{SCFR}=(G=(V,A),F,c,d)$ be a \textsc{SCFR}-instance. In the first step, we gradually transform $\I_{SCFR}$ into a \textsc{Co-Dominating Set}-instance. \\
\textbf{(i)}
Notice that we are not interested in the actual structure of the graph, but only in the distance between vertices and facilities. Therefore, we can pre-compute these distances (e.g. with Dijkstra's Algorithm in polynomial time) and create an alternative set of weighed undirected edges that only connect vertices to facilities, weighed with the according distances.
More formally, we define the transformed instance $\I_{SCFR}'=(G'=(V,E),F,c',d)$ with $E=V\times F$ and $c'(v,f):=d(v,f)$ for all $v\in V, f\in F$. Notice that our graph $G'$ is already bipartite with the according partition of vertices given by $V=F\cup V\setminus F =: U\cup W.$\\
Finally, remember that for each vertex $v\in V$, we only care about those facilities $f\in F$ that are reachable in time, i.e. $d(v,f)\leq d$. Therefore, we can further simplify our model by excluding all edges that exceed the reachability requirement, i.e. we set $E':=E\setminus\{(v,f)\in E| c'(v,f)>d\}$. \\
All these steps result in the final \textsc{Co-Dominating Set}-instance $\I_{CDS}=G_{CDS}=(U\cup W,E')$, which can be computed in polynomial time from $I_{SCFR}$. In the final step, we show the equivalence of optimality of both instances.\\
\textbf{(ii)}
Per construction, for any $f\in F$ and $v\in V$ we have $d(v,f)\leq d$ if and only if $\{v,f\}\in E'$. Therefore, for any $X\subseteq F(=U)$, we obtain
\begin{align*}
    &\max_{v\in V}d(v,X)<d \\
    \Leftrightarrow &\max_{v\in V}\min_{f\in X}d(v,f)<d \\
    \Leftrightarrow &\forall v\in V:\min_{f\in X}d(v,f)<d \\
    \Leftrightarrow &\forall v\in V:\exists f\in X: d(v,f)<d \\
    \Leftrightarrow &\forall v\in V\setminus F:\exists f\in X: \{v,f\}\in E' \\
    \Leftrightarrow &N(X)=V\setminus F (= W).
\end{align*}
showing that \textsc{SCFR} and \textsc{Co-Dominating Set} have exactly the same search space that we minimize over.
Therefore, $X$ solves $\I_{SCFR}$ if and only if it solves $\I_{CDS}$.
\end{proof}

In this chapter we have shown that the \emph{Constrained Facility Reduction Problem} - the main problem of this thesis - can be reduced to the much simpler \textsc{Co-Dominating Set} problem, which will therefore be the main object of study in the following chapters.

