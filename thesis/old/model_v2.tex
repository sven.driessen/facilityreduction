\section{Mathematical Models}
\subsection{Initial Model}
The first model of the problem is a direct translation into the language of graphs.
First we need to introduce some notation. \\
\begin{shaded}
\begin{notation}
Let $G=(V,A)$ be a directed graph with arc costs $c:A\rightarrow \R$. For $u,v\in V$, we define
    \[d(u,v) := \begin{cases} l(p),& \text{if there exists a shortest path } p \text{ from } u \text{ to } v \\
                            \infty,& \text{ if } u \text{ and } v \text{ are disconnected in }G
                \end{cases}
    \]
For $v\in V$ and a subset $W\subseteq V$, we define \[d(v,W):=\min_{w\in W}d(v,w)\]
\end{notation}
\end{shaded}
Now we can define the problem.
\begin{shaded}
\begin{definition}[Constrained Facility Reduction Problem (\textsc{CFR})] \hfill \\
    \textbf{Given:} A directed Graph $G=(V,A)$, arc costs $c:A\rightarrow \R$, a set of facilities $F\subseteq V$, reachability constraints $d_1\leq...\leq d_k \ (k\in \N)$.\\
    \textbf{Find:} A sequence $X=(X_1\supseteq...\supseteq X_k)$ of subsets $X_i\subseteq F$ such that for all $i\in\{1,...,k\}$, $X_i$ is a \emph{minimal} subset $X_i\subseteq X_{i-1}$ (with $X_0:=F$) satisfying $\max_{v\in V}(d(v,X_i))\leq d_i$.
\end{definition}
\end{shaded}
\textbf{Notes:}\begin{itemize}
\item We denote the search space by $\X$.
\item The indices $i\in\{1,...,k\}$ are called the \emph{facility levels}.
\item This problem can be seen as a multiobjective minimization problem with $k$ objective functions $c_1,...,c_k$ defined by $c_i(X)=|X_i|$. Multiobjective minimization problems are generally defined by non-dominating sets, and are very hard to solve. The kind of minimization used for this problem is different and much easier, since it allows for treating each objective function independently one after the other, as we will see soon. But on the other hand, this means that we treat the objective functions unequally, effectively prioritizing lower facility-levels. More precisely, the kind of minimization in this problem corresponds to a minimization of the objective function $c:\X\rightarrow \N^k, X\mapsto (|X_1|,...,|X_k|)$ using the lexicographical (total) order on $\N^k$. While this minimization conforms to the original problem definition in the Bertelsmann survey and it is easier to handle, studying the same problem as a multiobjective minimization problem using non-dominating sets is definitely an approach that might lead to very interesting results.
\end{itemize}

The problem can be broken down into several single-objective subproblems:

\begin{shaded}
\begin{lemma}\label{lemma:kto1}
A \textsc{CFR}-instance with $k$ facility levels can be solved by iteratively solving $k$ \textsc{CFR}-instances with $1$ facility level.
\end{lemma}
\end{shaded}
\begin{proof}
Let $\I=(G=(V,A),F,c,(d_1,...,d_k))$ be a \textsc{CFR}-instance with $k$ facility levels. Solving the instance $(G,F,c,d_1)$ with only 1 facility level yields a minimal subset $X_1\subseteq F$ with $\max_{v\in V}(d(v,X_1))\leq d_1$. Then $\I':=(G,X_1,c,(d_2,...,d_k))$ is an instance with $k-1$ facility levels. Repeating the above process inductively yields $X_2,...,X_k\subseteq F$ satisfying $X_i\subseteq X_{i-1}$ and $\max_{v\in V}(d(v,X_i))\leq d_i$ for $i\in\{2,...,k\}$. Then $X:=(X_1,...,X_k)$ is a solution to $\I$.
\end{proof}
As a result of this lemma, we can now assume that we have only one facility level, so we only need to find one minimal subset $X\subseteq F$ with $\max_{v\in V}d(v,X)\leq d$ for a given $d\in\R$. This version of \textsc{CFR} will from now on be called the \emph{Singular Contrained Facility Reduction Problem} (\textsc{SCFR}):
\begin{shaded}
\begin{definition}[Singular Constrained Facility Reduction Problem (\textsc{SCFR})] \hfill \\
    \textbf{Given:} A directed Graph $G=(V,A)$, arc costs $c:A\rightarrow \R$, a set of facilities $F\subseteq V$, a reachability constraint $d\in\R.$\\
    \textbf{Find:} A minimal subset $X\subseteq F$ with $\max_{v\in V}d(v,X)\leq d$.
\end{definition}
\end{shaded}
\textbf{Note:} In the language of complexity theory, Lemma~\ref{lemma:kto1} says that \textsc{CFR} is \emph{Turing reducible} to \textsc{SCFR}. In consequence, since \textsc{SCFR} as a subproblem is also Turing-reducible to \textsc{CFR}, both problems are \emph{Turing equivalent}.

%===============================================================

\subsection{Equivalence to \textsc{Set Cover}}
It turns out that \textsc{SCFR} can easily be transformed into the well-known NP-complete \textsc{Set Cover} problem. 
\begin{shaded}
\begin{definition}[\textsc{Set Cover} Problem] \hfill \\
\textbf{Given:} \begin{itemize}
    \item A set of elements $N$
    \item A collection $\mathcal{S}=\{S_1,...,S_m\}$ of subsets $S_j\subseteq N$ with $\bigcup\mathcal{S}=N$
\end{itemize}
\textbf{Find:} A \emph{minimal} set $\mathcal{S'}\subseteq\mathcal{S}$ such that $\bigcup\mathcal{S'}=N$ (called a \emph{set cover}).
\end{definition}
\end{shaded}

\begin{shaded}
\begin{definition}
Let $G=(V,A,c,F,d)$ be a \textsc{SCFR}-instance. Then we define the corresponding \textsc{Set Cover}-instance $\I_G=(N,\mathcal{S})$ as follows:
\begin{itemize}
    \item $N=V$
    \item $\mathcal{S}=\{S_f|f\in F\},$ where 
    \[S_f:=\{v\in V| d(f,v)\leq d\}\]
    for each $f\in F$.
\end{itemize}
\end{definition}
\end{shaded}
Intuitively, each facility $f$ can be identified with the set of vertices $S_f$ it covers, i.e. from which $f$ is reachable in time. We then seek a minimal set of facilities $X\subseteq F$ such that each vertex in the graph is covered by at least one, i.e.  $\bigcup_{x\in X}S_x=V$. \\
Note that the calculation of each $S_f$ can be done in time $O(|V|^2)$ using e.g. Dijkstra's Algorithm, and this computation needs to be done at most $|F|\leq |V|$ times. In total, the transformation $G\mapsto \I_G$ can be computed in time $O(|V|^3)$. Since this transformation only consists of distance computations that we can assume to be necessary in any algorithm solving \textsc{SCFR}, the time required to compute this transformation can be neglected. \\
The following Lemma follows directly from the above thoughts.
\begin{shaded}
\begin{lemma}\label{lemma:setcoverleqscfr}
\textsc{SCFR} $\leq_p$ \textsc{Set Cover}. Moreover, the reduction can be computed in $O(|V|^3).$
\end{lemma}
\end{shaded}
\begin{proof}
The only thing left to show is the equivalency of solutions. Let $G=(V,A,c,F,d)$ be a \textsc{SCFR}-instance and let $\I=(N,\mathcal{S})$ be the corresponding \textsc{Set Cover}-instance.
Then for any $X\subseteq F$ we have
\begin{align*}
    &\max_{v\in V}d(v,X) \leq d \\
    \Leftrightarrow\ &\text{For all $v\in V$ there exists an $x_v\in X$ with $d(x,v)\leq d$} \\
    \Leftrightarrow\ &\text{For all $v\in V$ there exists an $x_v\in X$ with $v\in S_{x_v}$} \\
    \Leftrightarrow\ &\bigcup\mathcal{S}=V. \\
\end{align*}
Therefore, $X$ solves $G$ if and only if it solves $I_G$.
\end{proof}
On the other hand, we can also reduce \textsc{Set Cover} to \textsc{SCFR} in polynomial time, making them ``equivalent'' in this sense.
\begin{shaded}
\begin{lemma}\label{lemma:scfrleqsetcover}
\textsc{Set Cover} $\leq_p$ \textsc{SCFR}. In particular, \textsc{SCFR} $\in\NPC$.
\end{lemma}
\end{shaded}
\begin{proof}
Let $\I=(N,\mathcal{S})$ be a \textsc{Set Cover}-instance. We define the corresponding \textsc{SCFR}-instance $G_\I=(V,A,c,F,d)$ as follows:
\begin{itemize}
    \item $F:=\{f_j|\ j=1,...,|\mathcal{S}|\}$ (auxiliary facilities)
    \item $V:= N \cup F$
    \item $A:= \{(v,f_j) |\ v\in S_j\}\subseteq V\times F $
    \item $c(a):=1$ for all $a\in A$
    \item $d=1$
\end{itemize}
Then for any $\mathcal{S}'\subseteq \mathcal{S}$ we define the corresponding set of facilities $X_{\mathcal{S}'}:=\{f_j\in F| S_j\in \mathcal{S}'\}$, and we obtain
\begin{align*}
    &\bigcup\mathcal{S}'= N \\
    \Leftrightarrow\ &\text{for each $i\in N$ there exists an $S_j\in\mathcal{S}'$ with $i\in\mathcal{S}'$} \\
    \Leftrightarrow\ &\text{for each $v\in V$, either $v\in F$ or there exists an $f_j\in X_{\mathcal{S}'}$ with $(v,f_j)\in A$} \\
    \Leftrightarrow\ &\text{for each $v\in V$, $d(v,X_{\mathcal{S}'})\leq 1$} \\
    \Leftrightarrow\ &\max_{v\in V}d(v,X_{\mathcal{S}'})\leq d. 
\end{align*}
Therefore, since $|\mathcal{S}'|=|X_{\mathcal{S}'}|$, $\mathcal{S}'$ solves $\I$ if and only if $X$ solves $G_\I$. \\
This proves \textsc{Set Cover} $\leq_p$ \textsc{SCFR}. Together with Lemma~\ref{lemma:setcoverleqscfr} and the fact that \textsc{Set Cover} $\in\NPC$ (as one of Karp's twenty-one NP-complete problems \cite{karp1972reducibility}) we finally obtain \textsc{SCFR} $\in\NPC$.
\end{proof} 

This chapter gave us two major insights. First, we are able to \emph{efficiently} transform our problem into the the well-known and well-researched \textsc{Set Cover} problem, allowing us to make use of various results that already exist on that. Secondly, we know that \textsc{Set Cover} is not harder than our problem (complexity-wise), ensuring that we do not accidentily make our problem way harder to solve by taking the re-route through \textsc{Set Cover}.







