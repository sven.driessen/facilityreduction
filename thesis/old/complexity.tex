\section{Complexity}
Before we start looking into finding algorithms to solve our problem, we want to assess how "hard" it is. If the problem turned out to be very simple, this thesis would either become very short or we might waste a lot of time developing heuristics, only to find out in the end that there would have been efficient exact algorithms all along. To prevent this, we analyze the complexity of the problem - and as it turns out, it is NP-hard and therefore "hard" to solve. \\
First, we will introduce the \textsc{Dominating Set} problem and prove that it is NP-complete using a reduction from \textsc{Set Cover}. Then we will reduce \textsc{Dominating Set} to \textsc{Co-Dominating Set}, finally showing that our problem is NP-complete.\\

\begin{shaded}
\begin{definition}[\textsc{Dominating Set} Problem] \hfill \\
\textbf{Given:} A Graph $G=(V,E)$. \\
\textbf{Find:} A \emph{minimal} set $X\subseteq V$ with $N(X)=V\setminus X$.
\end{definition}
\end{shaded}
\begin{shaded}
\begin{definition}[\textsc{Set Cover} Problem] \hfill \\
\textbf{Given:} \begin{itemize}
    \item A set of elements $N=\{1,...,n\}$
    \item A collection of subsets $\mathcal{S}=\{S_1,...,S_m\}$ with $\bigcup\mathcal{S}=N$
\end{itemize}
\textbf{Find:} A \emph{minimal} set $\mathcal{S'}\subseteq\mathcal{S}$ such that $\bigcup\mathcal{S'}=N$ (called a \emph{set cover}).

\end{definition}
\end{shaded}
We show that \textsc{Dominating Set}$\in\NPC$ using a reduction from $\textsc{Set Cover}$, which is one of the 21 problems shown to be NP-complete by Richard Karp in 1972 \cite{karp1972reducibility}.
\begin{shaded}
\begin{lemma}
\textsc{Dominating Set} $\in\NPC$
\end{lemma}
\end{shaded}
\begin{proof}
\begin{enumerate}
    \item \textsc{Dominating Set} $\in\NP:$\\
    Given a solution candidate $X$, verifying $N(X)=V\setminus X$ can be done in polynomial time. Therefore, \textsc{Dominating Set} $\in\NP$.
    \item \textsc{Set Cover} $\leq_p$ \textsc{Dominating Set}
\end{enumerate}
\end{proof}


\begin{shaded}
\begin{theorem}
\textsc{Co-Dominating Set} $\in\NPC$.
\end{theorem}
\end{shaded}
\begin{proof} \hfill \\
\begin{enumerate}
\item \textsc{Co-Dominating Set} $\in$ \NP: \\
Given a solution candidate $X$, verifying $N(X)=W$ can be done in polynomial time. Therefore, \textsc{Co-Dominating Set} $\in$ \NP.
\item \textsc{Dominating Set} $\leq_p$ \textsc{Co-Dominating Set}: \\
Let $G=(V,E)$ be a \textsc{Dominating Set}-instance. From this we construct a \textsc{Co-Dominating Set}-instance $G'=(V\cup V',E')$, where $V':=\{v'|v\in V\}$ is an exact copy of $V$ and $E':=\{\{v,w'\}|v=w \text{ or }\{v,w\}\in E\}.$ Then for any $X\subseteq V$ we obtain
\begin{align*}
    &N_{G'}(X)=V' \\
    \Leftrightarrow &\ \forall v\in V\setminus X\ \exists x\in X: \{x,v'\}\in E' \\
    \Leftrightarrow &\ \forall v\in V\setminus X\ \exists x\in X: \{x,v\}\in E \\
    \Leftrightarrow &\ N_G(X)=V.
\end{align*}
Therefore, $X$ solves $G'$ if and only if it solves $G$.
\end{enumerate}
Since \textsc{Dominating Set} $\in$ \NP, 1. and 2. yield that \textsc{Co-Dominating Set} is NP-complete.
\end{proof}
