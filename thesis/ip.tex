\section{Integer Program}
\label{sec:ip}

In this section, we will formulate the Constrained Facility Reduction problem as an Integer Program (IP).
Let $I=(V,F,d,p,(F_1,...,F_k),(d_1,...,d_k),\alpha)$ be an instance of CFR.
We use the indices $l\in\{1,...,n\}$ for locations $v_l\in V$, $j\in\{1,...,m\}$ for facilities $f_j\in F$ and
$i\in\{1,...,k\}$ for the facility levels. We represent the sets $F_1,...,F_k$ using the constants \[m_{ij}:=
\begin{cases}
    1,& f_j\in F_i\\
    0,& f_j\notin F_i
\end{cases}.\]
Based on this input, we model a feasible solution $(X_1,...,X_k)$ using the binary variables $x_{ij}$,
which we interpret as
\[x_{ij}=\begin{cases}
    1,& f_j\in X_i\\
    0,& f_j\notin X_i 
\end{cases}.\] 
In addition, we use auxiliary binary variables $z_{il}$ to mark which locations are covered on level $i$, which will be important to validate sufficient coverage.
We will refer to the variable space simply as $X$, although we will exclude the auxiliary variables for simplicity,
i.e. $X:=\{0,1\}^{m\cdot k}$. \\

We will now construct the Integer Program representing the instance $I$, beginning with the cost function. \\
If we allow our IP to contain multiple objective functions $u_i,i\in\{1,...,k\}$, we can simply define them as 
\[ u_i(x)=\sum_{j=1}^m x_{ij}\]
for all $i\in\{1,...,k\}$, where the lexicographical priorities are assumed to be descending.
However, sometimes it may be desirable to work with a single objective, for example if the accessible tools are limited and do not provide
a solver for multi-objective IPs. Therefore, we will now derive an alternative, equivalent single objective. \\
We need to find a real-valued linear function $c:X\rightarrow\R$ with
the property that $c(x^*)=\min_{x\in X}c(x)$ if and only if $x^*$ corresponds to an optimal solution $(X_1,...,X_k)$ of $I$, i.e. one that is 
lexicographically minimal. 
Therefore, we first need to find a \emph{strictly monotonic} linear function $f:(\N_0^k,<_{lex})\rightarrow(\R,<)$.
This function can then easily be converted into a corresponding function $c:X\rightarrow\R$.
Intuitively, one way to this is to interpret the vector $(|X_1|,...,|X_k|):=(x_1,...,x_k)$ that we want to minimize as the digits of a 
natural number in base $m+1$, which is possible since $|X_i|\in\{0,...,m\}$ for all $i\in\{1,...,k\}$. Minimizing this number in base $m+1$ 
then corresponds to lexicographically minimizing its digits, which is exactly what we want. Formally, we define the linear function
\[f:\{0,...,m\}^k\rightarrow\N_0, (x_1,...,x_k)\mapsto \sum_{i=1}^k x_i(m+1)^{k-i}\]
\begin{shaded}
    \begin{lemma}\label{lem:monotonicmap}
        The function 
        \[f:(\{0,...,m\}^k,<_{lex})\rightarrow (\N_0,<), (x_1,...,x_k)\mapsto \sum_{i=1}^k x_i(m+1)^{k-i}\] 
        is strictly monotonic.
    \end{lemma}
\end{shaded}
\begin{proof}
   Let $x=(x_1,...,x_k),y=(y_1,...,y_k)\in\{0,...,m\}^k$ with $x<_{lex}y$.
   Then there exists an $i_0\in\{1,...,k\}$ with $x_i=y_i$ for $i<i_0$ and $x_{i_0}<y_{i_0}$.
   Then
   \[\sum_{i=i_0+1}^k x_i(m+1)^{k-i} \leq m\sum_{j=0}^{k-i_0-1}(m+1)^j
   =m\frac{(m+1)^{k-i_0}-1}{m+1-1}=(m+1)^{k-i_0}-1\]
   yielding
   \begin{align*}
    f(x) =& \sum_{i=1}^{i_0-1}x_i(m+1)^{k-i} + x_{i_0}(m+1)^{k-i_0} + \sum_{i=i_0+1}^k x_i(m+1)^{k-i} \\
    \leq& \sum_{i=1}^{i_0-1}y_i(m+1)^{k-i} + (y_{i_0}-1)(m+1)^{k-i_0} + (m+1)^{k-i_0}-1 \\
    =& \sum_{i=1}^{i_0-1}y_i(m+1)^{k-i} + y_{i_0}(m+1)^{k-i_0} -1 \\
    =& \sum_{i=1}^{i_0}y_i(m+1)^{k-i} -1 \\
    <& \sum_{i=1}^{k}y_i(m+1)^{k-i} = f(y)
   \end{align*}
   as desired.
\end{proof}
Lemma~\ref{lem:monotonicmap} tells us that using the function $f$ as a cost function in the \textsc{CFR} problem
yields the same optimal solution. Therefore, we may use it in our IP-formulation. 
Reformulated as a cost function over the IP-variable space $X$ we obtain (using $|X_i|=\sum_{j=1}^m x_{ij}$)
\[u(x)=u((x_{ij})_{i,j})=\sum_{i=1}^k (m+1)^{k-i}\sum_{j=1}^m x_{ij} = \sum_{i=1}^k\sum_{j=1}^m (m+1)^{k-i}x_{ij}.\]
\\
Now that we have defined our cost function, we can begin defining the constraints for the IP. \\
The property $X_i\subseteq X_{i-1}\cap F_i$ can be modeled using the restrictions
\[x_{ij}\leq m_{ij} \ \forall i,j\]
and
\[x_{ij}-x_{(i-1)j} \leq 0 \ \forall i\geq 2,j.\]

We ensure that each level is sufficiently covered using
\[\sum_{l=1}^n p(v_l) z_{il} \geq (1-\alpha) p(V) \quad \forall i\]

Finally, for each facility level $i$ and each location $v_l\in V_i'$ (represented by $z_{il}$), 
there needs to exist a facility $f_j\in X_i$ with $d(v_l,f_j)\leq d_i$, which we can model by
\[\sum_{\substack{f_j\in F_i \\ v_l\in B_{d_i}(f_j)}} x_{ij} \geq z_{il} \quad \forall i,l.\]
\\
In conclusion, we obtain the following IP:
\begin{shaded}
    \begin{definition}[IP Formulation of Constrained Facility Reduction] \label{def:ip} \hfill \\
        Let $I=(V,F,d,p,(F_1,...,F_k),(d_1,...,d_k),\alpha)$ be an instance of \textsc{CFR}.
        Define $n:=|V|, m:=|F|$, and $m_{ij}:=
        \begin{cases}
            1,& f_j\in F_i\\
            0,& f_j\notin F_i
        \end{cases}.$
        Then the corresponding Integer Program Formulation is given by: \\
        \begin{align*}
            \min\quad &\sum_{i=1}^k\sum_{j=1}^m (m+1)^{k-i}x_{ij} \\
            \text{or} \\
            \min\quad& \sum_{j=1}^m x_{ij}, \quad i=1,...,k \\
            \text{s.t.} \quad &x_{ij}\leq m_{ij} \quad \forall i,j \\
            &x_{ij}-x_{(i-1)j} \leq 0 \quad \forall i\geq 2,j \\
            &\sum_{l=1}^n p(v_l) z_{il} \geq (1-\alpha) p(V) \quad \forall i\\
            &\sum_{\mathclap{\substack{f_j\in F_i \\ v_l\in B_{d_i}(f_j)}}} x_{ij} \geq z_{il} \quad \forall i,l\\
            &x_{ij},z_{il} \in\{0,1\} \quad \forall i\in\{1,...,k\},j\in\{1,...,m\},l\in\{1,...,n\}
        \end{align*}
    \end{definition}
\end{shaded}

With the IP-formulation of CFR, we are now able to obtain a lower bound for the optimal solution value, which we will
later use in the evaluation of our algorithms. In the next section, we will develop methods for instance optimization which
will allow us to obtain equivalent instances with a reduced size and complexity in a preprocessing stage before applying any 
solution heuristics.