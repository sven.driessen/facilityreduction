\section{Evaluation}
In this section, we will evaluate the algorithms formulated in the last sections. We will compare both their solution quality and time efficiency, using 
data obtained by applying the algorithms in multiple combinations to a sufficiently large and varied set of instances. All code for 
data aquisition, instance generation, algorithm implementation, testing and evaluation was written in Python and R.

\subsection{Instance Generation}
To be able to obtain general results, we need to generate a sufficiently large and varied set of instances. To this end, we set up a list of tweakable parameters,
which we can then combine to obtain many instances covering a wide range of properties.
\paragraph*{Geometry - $V,F,d,p$.}
The geometry of an instance consists of the positions (and population) of locations and facilities and their distances. These make up the core of the instance, and should be chosen
to best reflect real-world instances. Therefore, we choose to use use real geographical data for these parameters. More precisely, reflecting the problem
of hospital location planning in Germany, we choose four German states as example regions: Nordrhein-Westfalen, Bayern, Sachsen and Baden-Württemberg.
The details on how we obtain the relevant data and how we use it 
to generate the set of locations, hospitals, distances and population are described in section~\ref{cha:instance}.

\paragraph{Number of Levels - $k$.}
We choose three different values for the number of levels: one, two and five. $k=1$ represents the special case where there is only one level. Combined with 
$\alpha=0$, this would correspond to an instance of Root-CFR. Next, we have $k=2$ as our ``base-case'', corresponding to a division of hospitals into basic and 
maximum hospital care. Finally, the case $k=5$ is used to represent instances with a higher number of levels and therefore more restrictions.

\paragraph{Level Restrictions - $(F_1,...,F_k)$.}
Here we consider only two situations: Instances \emph{with} level restrictions and instances \emph{without} level restrictions.
In the first case we assign random levels such that $F=F_1\supsetneq...\supsetneq F_k$. While the actual choice of facilities for each level
is random, we use fixed level sizes $m_i:=|F_i|$ - concretely, we use 
\[(\frac{m_1}{m},...,\frac{m_k}{m})=
    \begin{cases} 
        (1)&,\text{if } k=1\\
        (1,0.25)&,\text{if }k=2\\
        (1,0.8,0.6,0.4,0.2)&,\text{if }k=5\\
    \end{cases}
\]
where $m:=|F|$. \\
In the second case we have $F_1=...=F_k=F$, which is also the case considered by the Bertelsmann survey. In this case, any facility is allowed to
potentially be of any level.

\paragraph*{Distance Restrictions and Level of Freedom - $(d_1,...,d_k), \beta$.}
When choosing the distance restrictions, we always need to make sure that the resulting instance is \emph{feasible}. If we choose the distance restrictions 
too tight, then we may not even be able to cover sufficiently many locations using the whole set of facilities. Hence, the distance restrictions have to be 
at least loose enough to allow sufficient coverage. More precisely, we define the \emph{base-restrictions} $(d_1^*,...,d_k^*)$ to be minimal such that 
$p(\bigcup_{f\in F_i}B_{d_i^*}(f))\geq(1-\alpha)p(V)$ still holds for all $i\in\{1,...,k\}$. Hence, for our instance to be feasible, we need $d_i\geq d_i^*$ for all $i\in\{1,...,k\}$.
If we choose $(d_1,...,d_k)=(d_1^*,...,d_k^*)$, we do not allow our solution to be ``less reachable'' than the status quo - instead, we only want it to
minimize overcapacities, or ``overlap'' in the regions covered by the facilities, while maintaining the current level of overall reachability of the facilities.
On the other hand, if we want to allow our solution to become ``less reachable'' overall, we may choose $d_i>d_i^*$, effectively giving the potential solutions
more \emph{freedom}. Therefore, for $F_1\supsetneq...\supsetneq F_k$ we choose $d_i=\lceil \beta d_i^*\rceil$ with a varying \emph{freedom factor} 
$\beta\in\{1,1.5\}$, to represent both of the cases above.  \\
Note that if $F_1=...=F_k$, then $d_1^*=...=d_k^*$. This case only makes sense if we give higher levels \emph{more freedom}, since they do not come with 
any restrictions themselves. Therefore, in this case we manually choose $(d_1,...,d_k)=\beta(\beta_1 d_1^*,...,\beta_k d_k^*)$ with base-freedom-parameters
\[(\beta_1,...,\beta_k)=
    \begin{cases} 
        (1)&,\text{if } k=1\\
        (1,2)&,\text{if }k=2\\
        (1,1.25,1.5,1.75,2)&,\text{if }k=5\\
    \end{cases}
\]
and the general freedom-parameter $\beta\in\{1,1.5\}$ as before. \\

Note that in all cases, $(d_1,...,d_k)$ depends on the instance, i.e. on the other parameters!

\paragraph*{Margin of Error - $\alpha$.}
The main purpose of the margin of error is to account for outliers in the status quo. If, for example, there is a single location in the data that is much 
farther away from the nearest facility than all the other locations, then we do not want to greatly loosen our restrictions only to account for that one outlier.
Therefore we allow for a small margin of error in the coverage of the locations. The purpose of the margin of error is generally \emph{not} to provide a level of 
freedom to the solution - this is the purpose of the distance restrictions themselves, as described in the last paragraph. We therefore only choose a 
single appropiate value for the margin of error, $\alpha = 0.05$.

\paragraph*{}
A summary of all parameters defining the set of instances we use for the evaluation is shown in table~\ref{tab:instance_parameters}. Taking into account that 
for $k=1$ there all level restriction options are equal, we obtain a total of 40 unique instances.
\begin{table}[h!]
    \centering
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        \textbf{Geometry} & \textbf{Levels} & \textbf{Level Restrictions} & \textbf{Freedom} & \textbf{Margin of Error} \\
        $(V,F,d,p)$ & $k$ & $(F_1,...,F_k)$ & $\beta$ & $\alpha$ \\
        \hline
        Nordrhein-Westfalen & 1 & $F_1=...=F_k$ & 1 & 0.05 \\
        Bayern & 2 & $F_1\supsetneq ... \supsetneq F_k$ & 1.5 & \\
        Sachsen & 5 & & & \\
        Baden-Württemberg & & & & \\
        \hline
    \end{tabular}   
    \caption{Parameters for instance generation. Yields a total of 40 unique instances.}
    \label{tab:instance_parameters}
\end{table}

Furthermore, we classify the instances by their \emph{instance type}, defined by the combination of all parameters except their geometry. The enumeration of 
instance types is shown in Table~\ref{tab:types}.

\begin{table}[h!]
    \centering
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        \textbf{Type} & \textbf{Levels} & \textbf{Level Restrictions} & \textbf{Freedom} & \textbf{Margin of Error} \\
        \hline
        1 & 1 & $F_1=...=F_k$ & 1 & 0.05 \\
        2 & 1 & $F_1=...=F_k$ & 1.5 & 0.05 \\
        3 & 2 & $F_1=...=F_k$ & 1 & 0.05 \\
        4 & 2 & $F_1=...=F_k$ & 1.5 & 0.05 \\
        5 & 2 & $F_1\supsetneq ...\supsetneq F_k$ & 1 & 0.05 \\
        6 & 2 & $F_1\supsetneq ...\supsetneq F_k$ & 1.5 & 0.05 \\
        7 & 5 & $F_1=...=F_k$ & 1 & 0.05 \\
        8 & 5 & $F_1=...=F_k$ & 1.5 & 0.05 \\
        9 & 5 & $F_1\supsetneq ...\supsetneq F_k$ & 1 & 0.05 \\
        10 & 5 & $F_1\supsetneq ...\supsetneq F_k$ & 1.5 & 0.05 \\
        \hline
    \end{tabular}
    \caption{Classification of instances into instance types}
    \label{tab:types}
\end{table}


\subsection{Algorithms}
Similar to the set of instances, we obtain a set of procedures by combining the different algorithms introduced in the last section in a pipelined fashion.
\paragraph*{Stage 1: Static Reduction.}
At the beginning, we either decide to perform a static reduction using Anchor Deletion or Area Sweep, resulting in a new, static instance,
or we keep our dynamic instance as-is. The former option allows for further instance optimization, while the latter gives the final algorithms more freedom.

\paragraph*{Stage 2: Instance Optimization.}
Whether the last stage yields a static or dynamic instance, in both cases we can start by computing the set of non-dominated facilities.
Only then do we need to make a distinction. If the instance is dynamic, we may only reduce our location set via coverage reduction. If it is static, however, we may
instead perform the stronger reduction by elimination of dominated locations. While the latter is certainly stronger than the former, we nevertheless 
perform both reductions to compare the results.

\paragraph*{Stage 3: Essential Facilities.}
If the previous stage yields a static instance, we may pre-compute essential facilities to give the final algorithms a headstart.
However, we also pass on the orgininal instance without the pre-computed essential facilities to compare the results.

\paragraph*{Stage 4: Solution Heuristics.}
In this final stage, we apply our three solution heuristics: Top-Down Cover, Bottom-Up Cover and Balanced Cover, to obtain the final results.

\paragraph*{LP Relaxation.}
In addition to the pipelined algorithms above, for each instance we compute the solution to the LP-relaxation of the Integer Program formulated in 
section~\ref{sec:ip} to obtain a lower bound on the optimal solution value. We will use this lower bound to compute the \emph{gap} of our heuristic
solution values.

\paragraph*{}
In summary, we obtain a total of 27 combinations of algorithms to perform for each instance, plus the additional relaxed IP computation.
In the following subsection, we will analyze the results obtained from this setup to determine the efficiency and quality of each algorithm.

\subsection{Results}
We will begin by analyzing the results for the static reduction heuristics.
\subsubsection{Static Reduction Heuristics}
\paragraph*{}
The main purpose of the static reduction heuristics is to transform a dynamic instance into a static one in order to allow for the subsequent elimination of
dominated locations and the calculation of essential facilities. This potentially results in a much smaller instance that is much more efficient to solve, which 
might be an important factor if the original instance is very large, time-resources are very limited or we want to apply further calculations after the heuristic 
solution is obtained, as for example a local search. As such, we choose to observe the effect of the static reduction on the computation time of the final 
solution heuristics, comparing the use of anchor deletion, area sweep and not using any static reduction at all. The results can be seen in 
Figure~\ref{fig:static_reduction_final_time}. We can clearly see that using any of the static reductions yields a much lower computation time of the final
solution heuristics. However, we also see that the difference between the effects of anchor deletion and area sweep is negligible. This was to be expected,
since the performance effects stem mainly from the improvements made possible by the static reduction, rather than the computed static location set itself.\\

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.6\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/eval/static_reduction_final_time_all.png}
        \subcaption{including no reduction}
    \end{subfigure}
    \begin{subfigure}{0.6\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/eval/static_reduction_final_time_two.png}
        \subcaption{only including reductions}
    \end{subfigure}
    \caption{Comparison of the effect of different static reduction heuristics on the computation time of the final solution heuristics (including essential facility
    calculation)}
    \label{fig:static_reduction_final_time}
\end{figure}

\paragraph*{}
While the effect of the static reductions on the performance of the final solution heuristics is as desired, they do come at a high cost. By performing a static
reduction, we fix a \emph{single} set $V'\subseteq V$ which has to be covered on all levels, while the dynamic instance allows for different sets $V_i'$ for each
level $i$, given the solution (and the solution heuristics) much more freedom. As such, we can expect a drop in solution quality when performing any static 
reduction. As a metric for measuring the solution quality, we choose the \emph{gap}, which measures the relative ``distance'' from the solution value to
the lower bound obtained by solving the LP-relaxation of the problem. It is defined as 
\[\gap(x_i):=\frac{x_i-x^{lp}_i}{x^{lp}_i}\]
where $x=(x_1,...,x_k):=(|X_1|,...,|X_k|)$ is value of the solution $X$ and $x^{lp}$ is the corresponding LP-relaxed solution value. To evaluate the solution quality, 
we choose the gap in the first partial solution $X_i$, i.e. $\gap(x_1)$, as minimizing $x_1$ is the main objective.
The results can be seen in Figure~\ref{fig:static_reduction_quality}. The results confirm that the solution quality when not using static reduction is much higher, 
and also more stable. The solution quality of both static reduction heuristics is more widely spread out. While range of ``good'' values (i.e. the lower part of the 
boxplot) is very similar, area sweep has a much wider spread of ``bad'' values, and a higher median than anchor deletion. Therefore, anchor deletion is more stable 
and overall slightly better than area sweep in term of their solution quality. \\

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{img/eval/static_reduction_quality.png}
    \caption{Solution quality depending on the use of static reduction heuristics, measured using the gap in the first partial solution value, $\gap(x_1)$.}
    \label{fig:static_reduction_quality}
\end{figure}

\paragraph*{}
Finally, we compare the computation time of the static reduction heuristics themselves. The results can be seen in Figure~\ref{fig:static_reduction_time}.
We can see that the difference in computation time of both heuristics is similar to their difference in solution quality. Again, anchor deletion is more stable 
in its upper range of values, and has an overall slightly better compuation time. Note that the future solution computation time saved by performing the static 
reduction is - at least in our case - largely negated by the additional computation time required for computing the static reduction itself. However, as mentioned 
earlier, this might change for larger instances or more demanding calculations performed on the instance after the reduction.\\

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{img/eval/static_reduction_time.png}
    \caption{Computation time of the static reduction heuristics.}
    \label{fig:static_reduction_time}
\end{figure}

\paragraph*{}
In conclusion, performing any static reduction on the instance allows for improvements that make subsequent computations much faster, but comes at the cost 
of a drop in solution quality. If we decide to perform a static reduction, the anchor deletion heuristic is the better choice considering both the solution 
quality and the computation time.


\subsubsection{Instance Optimization}
While in the last section we have already indirectly seen how different levels of instance optimization affect the computation time of the final solution heuristics,
we will now analyze the results of the different forms of instance optimization in more detail.

\paragraph*{Facility Optimization}
The results of the facility optimization can be seen in Figure~\ref{fig:instance_opt_facilities}. The computation can take up to 8 seconds, with three-fourths of cases 
taking less than four seconds. The number of facilities could be reduced by up to twelve percent, but in about half of the cases, the number of 
nondominated facilities lies between 91 and 97 percent. 


\begin{figure}[H]
    \centering
    \begin{subfigure}{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/eval/instance_opt_facilities_time.png}
        \subcaption{Computation time \\ \hspace{1cm}}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/eval/instance_opt_facilities_quality.png}
        \subcaption{Relative number of nondominated facilities}
    \end{subfigure}
    \caption{Results of the computation of nondominated facilities}
    \label{fig:instance_opt_facilities}
\end{figure}


\paragraph*{Location Optimization}
The results for the location optimization can be found in Figure~\ref{fig:instance_opt_locations_quality} and Figure~\ref{fig:instance_opt_locations_time}.
Immediately we notice that even with coverage classes alone we can achieve a much greater reduction than in the case of facilities, with the median lying 
at approximately 50\%. If we are able to use the reduction by eliminating dominated locations, the reduction is much greater, with a median as low as about 4\% and a 
relatively stable range of values. These results explain the large improvement in the solution heuristic computation time observed when performing a static
reduction, which allows the elimination of dominated locations. \\
However, the tradeoff lies in a very high computation time with a median at 4 seconds, an upper quantile at 14 seconds, and a maximum at 27 seconds. The time 
required for computing the coverage classes is just a little lower in its median, but much more stable, with an upper quantile at 3 seconds and a maximum at 6 seconds.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{img/eval/instance_opt_locations_quality.png}
    \caption{Number of nondominated locations and coverage classes}
    \label{fig:instance_opt_locations_quality}
\end{figure}

\paragraph*{}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{img/eval/instance_opt_locations_time.png}
    \caption{Computation time for nondominated locations and coverage classes}
    \label{fig:instance_opt_locations_time}
\end{figure}

\paragraph*{}
In conclusion, it becomes clear that while we are able to achieve a very high level of optimization, especially using elimination of dominated locations,
the cost in computation time may also become very high. Therefore, performing these optimizations (and consequently a static reduction) is only viable if the instance
will be subject to very time-consuming subsequent computations whose potential computation time outweighs the cost for the initial instance optimization.

\subsubsection{Essential Facilities}
The results of the computation of essential facilities are depicted in Figure~\ref{fig:essential_facilities}.
Note that essential facilties may only be computed in static instances, i.e. in our case if we performed a static reduction. For this reason,
we split the result into the cases where the static instance was obtained by anchor deletion and area sweep. \\
We see that the number of essential facilities varies a lot. In the cases where anchor deletion was used, there are often no or only very few essential facilities left, as
the corresponding anchors of many or all essential facilities have been removed. Nevertheless, with a median at around 30\%, the number of essential facilities that are pre-computed
is still significant.
In the cases where area sweep was used as static reduction, the range of values is much narrower, centered around the median of around 55\%. \\
Luckily, the calculation of essential facilities is very efficient, with a maximum at 0.14 seconds and a median of 0.02 seconds. \\

Overall, pre-calculating essential facilities turns out to be very effective, although it requires a static instance to be applicable.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/eval/essential_facilities_quality.png}
        \subcaption{Relative number of essential facilities in the solution depending on static reduction heuristic}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/eval/essential_facilities_time.png}
        \subcaption{Computation time \\ \hspace{1cm}\\ \hspace{1cm}}
    \end{subfigure}
    \caption{Results of the computation of essential facilities}
    \label{fig:essential_facilities}
\end{figure}

\subsubsection{Solution Heuristics}
The results for the solution heuristics are depicted in Figure~\ref{fig:solution_heuristics_quality} and Figure~\ref{fig:solution_heuristics_time}.
\paragraph*{}
Figure~\ref{fig:solution_heuristics_quality} depicts the average solution quality of each heuristic, seperated into different instance types. Note that 
this figure \emph{only includes data from dynamic instances}, as the static reduction and the pre-calculation of essential facilities heavily affects 
the instances and skews the results of the solution heuristics. To compare the pure solution heuristics, restricting ourselves to the dynamic instances, 
which are unmodified except for some location optimization, should yield more accurate results. \\
Somewhat suprprisingly, it becomes immediately apparent that the three solution heuristics seem to be nearly identical in their solution quality - not only 
on average, but even when evaluated for each instance type individually. The quality of the balanced heuristic seems to be consistenly very slightly worse than
the other two. The top-down and bottom-up heuristics fluctuate in which one succeeds over the other, however they are always very close, and there is no
immediate pattern discernible. Also note that since we broke up the solution quality evaluation into 40 different values and restricted ourselves to
dynamic instances, each value contains only 4 averaged observations, thereby producing some natural fluctuations due to the lack of samples. \\

One interesting observation is that the even instance types, which are exactly those with a freedom factor of 1.5 instead of 1, have a consistenly worse solution
quality. One likely explanation goes back to the original greedy heuristic for R-CFR as introduced in Section~\ref{sec:algorithms}, specifically Lemma~\ref{lem:approx}.
A higher freedom factor results in looser distance restrictions, and thereby larger facility-covers $B_{d_i}(f)$. By Lemma~\ref{lem:approx}, this increases the 
approximation ratio of the greedy algorithm for this specific instance, leading to a \emph{potentially} worse solution quality.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{img/eval/solution_heuristics_quality_plot.png}
    \caption{Solution quality of solution heuristics dependent on instance type. Only includes data from dynamic instances.}
    \label{fig:solution_heuristics_quality}
\end{figure}

\paragraph*{}
While the three heuristics are nearly undiscernible in their solution quality, Figure~\ref{fig:solution_heuristics_time} makes clear that while 
the top-down and the balanced heuristic are also nearly identical in their computation time, the top-down heuristic seems to be much more efficient than the other two.

\paragraph*{}
Overall, it seems like the top-down heuristic wins over the other two, as it has a better time efficiency while producing results that are 
just as good as those from the other heuristics. This is somewhat surprising considering the unintuitive design of the top-down heuristic, as it
favors facilities that are best suited for \emph{higher} levels. This possibly implies that the coverage on higher levels, i.e. coverage using a larger radius,
is in fact a good utility metric for lower level facilities. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{img/eval/solution_heuristics_time_complete.png}
    \caption{Computation time of solution heuristics}
    \label{fig:solution_heuristics_time}
\end{figure}


\subsubsection{Conclusion and Example}
From the results we have seen in this section, we can conclude that the optimal choice of which combination of algorithms we choose to solve the 
problem depends heavily on our purpose. If we place a high value on a highly optimized instance and fast solution heuristics while having preprocessing time
to spare and if we are okay with a slightly worse solution quality, performing a static reduction using Area Sweep is the best choice. We should then 
eliminate dominated facilities as well as locations and finally compute the solution by first precomputing all essential facilities and then computing 
the remaining solution using the top-down heuristic, which produces results that are just as good as using the bottom-up or balanced heuristics, but 
with a faster computation time. \\
On the other hand, if we value a good solution quality above all else, we should keep the dynamic instance, eliminate dominated facilities, perform the 
coverage reduction, and finally compute the solution using the top-down heuristic.

\subsubsection*{Example}
To conclude, will show an example instance and two ways of solving it. We choose the Nordrhein-Westfalen instance with $k=2$ levels, level restrictions $F_1\supsetneq F_2$,
a freedom factor of $\beta=1$ and the margin of error $\alpha=0.05$. The instance with its original grid-cell locations and a visualization of its population distribution 
are shown in Figure~\ref{fig:instance}.

\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \includegraphics*[width=\textwidth]{img/example/instance.png}
        \subcaption{instance}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \includegraphics[width=\textwidth]{img/example/instance_sampled.png}
        \subcaption{population distribution}
    \end{subfigure}
    \caption{Nordrhein-Westfalen instance with $k=2, F_1\supsetneq F_2, \beta=1$ and $\alpha=0.05$, including the instance itself and a visualization of the population
    distribution.}
    \label{fig:instance}
\end{figure}

First, we solve the instance \emph{dynamically}, i.e. we only apply the reduced set of optimizations including the elimination of dominated facilities and the 
coverage reduction, and then we apply the top-down heuristic to obtain the final solution. This process is shown in Figure~\ref{fig:dynamic_solve}.

\begin{figure}[h!]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/example/reduced_opt.png}
        \subcaption{optimized instance}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/example/solution_dynamic.png}
        \subcaption{final solution}
    \end{subfigure}
    \caption{The optimized instance after eliminating dominated facilities and applying the coverage reduction,
    and the final solution obtained using the top-down heuristic.}
    \label{fig:dynamic_solve}

\end{figure}

Finally, we solve the instance \emph{statically}, i.e. we perform a static reduction using area sweep, then we optimize the instance using the \emph{full} optimization set 
including elimination of dominated
facilities and dominated locations, then apply the top-down heuristic including the pre-calculation of essential facilities to obtain the final solution. This process 
is shown in Figure~\ref{fig:static_solve}.

\begin{figure}[h!]
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/example/full_opt.png}
        \subcaption{optimized instance}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{img/example/solution_static.png}
        \subcaption{final solution}
    \end{subfigure}
    \caption{The optimized instance after eliminating dominated facilities and dominated locations, 
    and the final solution obtained using pre-calculation of essential facilities and the top-down heuristic.}
    \label{fig:static_solve}
\end{figure}