\section{Trees}
Before we analyze CFR in its general form, 
we will consider an interesting special case where the problem instance is a \emph{tree}, or - more generally - a \emph{subset} of a tree.
Precisely, we restrict the problem by requiring that $V,F\subseteq V(T)$ and $d=d_T$ for a \emph{weighed tree} $T=(V(T),E,c)$, 
where $c:E\rightarrow \R_{\geq 0}$ defines the cost of each edge $e\in E$.
Here, $d=d_T$ refers to the shortest-path-distance in $T$, i.e. for any $u,v\in V$, $d(u,v)=d_T(u,v)=c(p_{[u,v]})$ 
where $p_{[u,v]}:u\rightarrow v$ denotes the unique shortest path between $u$ and $v$ in $T$. \\
Additionally, we require that $\alpha=0$ and that there are no level restrictions, i.e. $F_1=...=F_k=F$.

As it turns out, the simple structure of trees results in a very nice property that will make the finding of an optimal solution much easier.
The following lemma says that for a well-chosen location $v$, we can find a facility $f^*$ covering $v$ that is optimal in the sense that its
cover $B_{d_i}(f^*)$ already includes all other possible covers covering $v$, i.e. $f^*$ is a total maximum if we order facilites covering $v$ 
by their cover via inclusion. Such an $f^*$ is \emph{guaranteed} to be part of an optimal solution. \\

Before formulating the lemma, we introduce some concepts and notation. Let $d\geq0$. In addition to the already defined cover $B_d(f)$ for $f\in F$,
we analogously define $B_d(v):=\{f\in F| d(f,v)\leq d\}$ for $v\in V$. To avoid confusion, we use the alternative notation $F_d(v):=B_d(v)$ 
for locations.\\
If a root $r$ for $T$ is specified, we call $T$ a \emph{rooted} tree.
The root defines a depth $d(v):=d(r,v)$ for all $v\in V(T)$. We can now orient the tree by orienting all edges away from the root.
More precisely, any unoriented edge $\{u,w\}$ becomes the oriented edge $(u,w)$ if $d(u)<d(w)$ or $(w,u)$ otherwise. For an oriented
edge $(u,v)$, we call $u$ the \emph{parent} and $v$ the \emph{child}. If $u$ and $v$ are connected by an oriented path $p:u\rightarrow v$,
we call $u$ an \emph{ancestor} of $v$ and $v$ a \emph{descendant} of $u$. We can define the subtree $T[u]$ as the subtree induced by all
descendants of $u$. For any subset $U\subseteq V$ of vertices, we can define their \emph{nearest common ancestor} $z=\nca(U)$ as the unique vertex
$z\in V(T)$ with $U\subseteq T[z]$ and maximal depth $d(z)$. Note that while all these definition are made using the induced orientation of
$T$ given its root, $T$ itself is still \emph{undirected}.
\begin{shaded}
    \begin{lemma}\label{lem:tree}
        Let $T$ be a weighed, rooted tree, $V,F\subseteq V(T)$ and $d\in\R_{\geq 0}$. 
        Let $v\in V$ have maximum depth $d(v)$. 
        Then there exists an $f^*\in F_d(v)$ with $B_{d'}(f)\subseteq B_{d'}(f^*)$ for all $f\in F_d(v)$ and all $d'\geq d$.
    \end{lemma}
\end{shaded}
\begin{proof}
    Define $z\in V(T)$ as the nearest common ancestor of all $f\in F_d(v)$, i.e. $z:=\nca(F_d(v))$. 
    We choose $f^*\in F_d(v)$ such that $d(f^*,z)$ is minimal. See Figure~\ref{fig:tikz_tree} for an example of this setup.
    Now let $d'\geq d, f\in F_d(v)$ and $w\in B_{d'}(f)$.
    We want to show that $w\in B_{d'}(f^*)$. For this, we make a case distinction. First, assume $w\in T[z]$. Since $z$ is the nearest common
    ancestor of $F_d(v)$, there exists an $f'\in F_d(v)$ with $z\in p_{[f',v]}$. Therefore, $d(f',v)=d(f',z)+d(z,v)$. It follows that
    \[d(f^*,w)\leq d(f^*,z)+d(z,w)\leq d(f',z)+d(z,v) = d(f',v) \leq d \leq d'\] 
    where the seconds inequality holds because $d(f^*,z)\leq d(f',z)$ per definition of $f^*$ and
    $d(z,w)=d(r,w)-d(r,z)\leq d(r,v)-d(r,z)=d(z,v)$ because $w,v\in T[z]$ and $d(v)\geq d(w)$. Therefore, $w\in B_{d'}(f^*)$ as desired. \\
    Now assume $w\notin T[z]$. Then for \emph{all} $f'\in F_d(v)$ it holds 
    that $z\in p_{[f',w]}$ and therefore $d(f',w)=d(f',z)+d(z,w)$. It follows that
    \[d(f^*,w)=d(f^*,z)+d(z,w)\leq d(f,z)+d(z,w)=d(f,w)\leq d'\] 
    and therefore $w\in B_{d'}(f^*)$, as desired. 
    
\end{proof}

\begin{figure}[h!]
    \centering 
    \input{tikz/tree_example.tex}
    \caption{An example of an unweighed tree (i.e. $c(e)=1$ for all edges $e$) with locations $V=\{v_1,...,v_5\}$, facilities $F=\{f_1,...,f_5\}$ and $d=3$. For the location $v$ with highest depth $d(v)=3$ we 
    obtain $F_d(v)=\{f_3,f_4,f_5\}$ with nearest common ancestor $z=v_2$ and the resulting nearest facilitiy $f^*=f_4$. Note that $f^*=f_3$ would have been an 
    equally valid choice.}
    \label{fig:tikz_tree}
\end{figure}
While the above lemma only makes a statement about a single vertex $v$, we can use it to formulate a simple iterative algorithm. 
To see its importance, we need to investigate what makes finding an optimal solution difficult. One 
thing we know for sure is that (in the case $\alpha = 0$) every location needs to be covered by at least one facility. However generally, 
when choosing a facility to cover a single location $v$, we have multiple options, and we have no way of deciding which option
to choose in order to arrive at an optimal solution in the end. Choosing one option over the other might prove to be a mistake later on, forcing
us to backtrack, which results in an exponential runtime. What Lemma~\ref{lem:tree} tells us is that there is a facility which is ``best'' in the
sense that we \emph{cannot make a mistake} by choosing it over any other facility covering $v$, since its covered area \emph{includes} those
of every other option. But when we choose this facility $f^*$ over the other facilites, we place a restriction on the facilities of higher levels,
because these need to be a subset of the facilities chosen for lower levels. What if $f^*$ is locally optimal for level 1, 
but some other facility $f'$ was better at
some higher level $i$, i.e. $B_{d_i}(f')\not\subseteq B_{d_i}(f^*)$? Fortunately, Lemma~\ref{lem:tree} covers this potential problem as well,
since the property holds not only for $d_1$, but \emph{for all $d\geq d_1$ as well}. This way, we can ensure global optimality of $f^*$ concerning
the coverage of $v$. But what about the other vertices? Fortunately, once we have determined $f^*$ - which is guaranteed to be contained in an optimal
solution - we know that all vertices in the range of $f^*$ are already covered.
Therefore, we can safely ignore all vertices covered by $f^*$ from now on and apply the lemma on the remaining vertices. However,
note that the remaining vertices do not necessarily form a tree. Deleting $B_d(f^*)$ from the tree $T$ might result in multiple disconnected components,
which might pose a problem if a vertex in one component is dependent on a facility in another. Therefore, we do not want to actually delete $B_d(f^*)$
from $T$. Instead, we only change our instance $I$ by removing $B_d(f^*)$ from its set of locations 
while leaving the distance function $d=d_T$ unchanged. Additionaly, since we know that each facility $f\in F_d(v)$ cannot cover any vertices 
outside $B_d(f^*)$, we can also safely remove $F_d(v)$ from the set of facilities.



\begin{shaded}
    \begin{algorithm}[H]\label{alg:tree}
        \caption{Location Elimination Algorithm for Trees}
        \textbf{Input:} A \textsc{CFR} instance $I=(V,F,(F_1=...=F_k=F),d,(d_1,...,d_k),\alpha=0)$
            with $V,F\subseteq V(T)$ and $d=d_T$ for a weighed tree $T$. \\
        \textbf{Output:} A solution $X=(X_1,...,X_k)$ to $I$.
        \begin{algorithmic}[1]
            \State $r \gets v\in V$ (arbitrary)
            \State $X_1,...,X_k \gets \emptyset $
            \For{$v\in V$} \State $d(v) \gets d(r,v)$ \EndFor
            \For{$i=1,...,k$}
                \State $d\gets d_i, V'\gets V, F'\gets F_i\cap X_{i-1}$ (with $X_0:=F$)
                \While{$V'\not=\emptyset$}
                        \State $v \gets \argmax_{v'\in V'}d(v')$
                        \State $F_d(v)\gets \{f\in F'| d(f,v)\leq d\}$
                        \State $z \gets \nca(F_d(v))$
                        \State $f^* \gets \argmin_{f\in F_d(v)}d(f,z)$
                        \State $X_i \gets X_i\cup\{f^*\}$
                        \State $V' \gets V'\setminus B_d(f^*)$
                        \State $F' \gets F'\setminus F_d(v)$.
                    \EndWhile
            \EndFor
            \Return $X=(X_1,...,X_k)$
        \end{algorithmic}
    \end{algorithm}
\end{shaded}

\begin{shaded}
    \begin{theorem}[Correctness]
        Algorithm~1 calculates an optimal solution.
    \end{theorem}
\end{shaded}
\begin{proof}
    We use the terminology used in the algorithm. \\
    \textbf{(i) Local optimality. }
    First we show the local optimality for each $X_i$, i.e. that $X_i$ is minimal for fixed $X_{i-1}$. Let $i\in\{1,...,k\}$ be arbitrary. 
    We will prove by induction over the number of locations $|V|$ that the $i-th$ iteration of the for-loop produces a locally optimal partial solution $X_i$. 
    For $|V|=1$, the while-loop always terminates after one iteration and thus the algorithm returns a solution with $|X_i|=1$ for each $i\in\{1,...,n\}$, 
    which is optimal.\\ 
    Now let $|V|\geq 2$. Then we obtain $f^*$ as in Lemma~\ref{lem:tree}. Since $v\in B_{d_i}(f^*)$, it holds that 
    \[|V'|=|V|-|B_{d_i}(f^*)|\leq |V|-1.\] 
    According to Lemma~\ref{lem:tree} all $f\in F_{d_i}(v)$ satisfy $B_{d_i}(f)\subseteq B_{d_i}(f^*)$, so each location $w\in V'=V\setminus B_{d_i}(f^*)$ needs to 
    be covered by an $f\in F\setminus F_{d_i}(v) = F'$. 
    Therefore, since we assumed that a solution exists and therefore all locations need to be covered, it holds that
    \[\bigcup_{f\in F'}B_{d_i}(f) = V'.\] 
    Therefore, an optimal solution exists for the reduced instance $I'=(V',F',\linebreak[1](F_1,...,F_k),\linebreak[1]d,(d_1,...,d_k),\alpha=0)$ 
    which is the input for the remaining while-loop
    after the first iteration. Per induction, the remaining while-loop gives us a locally optimal solution $Y_i=X_i\setminus\{f^*\}$ for $I'$.
    Since $F'=F\setminus F_{d_i}(v)$ and $B_{d_i}(f)\cap V'=\emptyset$ for all $f\in F_{d_i}(v)$, $Y_i$ is also locally
    optimal for the instance $I''=(V',F,(F_1,...,F_k),d,(d_1,...,d_k),\alpha=0)$ (i.e. where only $V'$ differs from $I$). 
    Per construction it holds that 
    \[v\notin \bigcup_{f\in F'}B_{d_i}(f)\] 
    and therefore $X_i=Y_i\cup\{f^*\}$ is locally optimal for $I$ (Otherwise, given a locally optimal solution $X_i^*$ for $I$, 
    remove the facility covering $v$ to obtain a better solution for $I''$.) \\
    \textbf{(ii) Global optimality. }
    To see that $X$ is \emph{globally optimal}, we show per induction that for each $i=0,...,k$, $X^{(i)}=(X_1,...,X_i)$ (with $X^{(0)}:=\emptyset$ and $X_0:=F$) 
    can be extended to an optimal solution. For $i=0$, this is trivial since a valid instance implies that a solution exists. Now let $i_0>0$. Then per induction
    there exists an optimal solution $X^*$ with $X_i^*=X_i$ for all $i<i_0$. Now let $g\in X_j^*$ for any $j\geq i_0$. 
    Then by our choice of $f^*$ in the algorithm (using Lemma~\ref{lem:tree})
    there exists an $f^*_g\in X_{i_0}$ with $B_{d_{i_0}}(g)\subseteq B_{d_{i_0}}(f^*_g)$. Lemma~\ref{lem:tree} then also implies that
    $B_{d_j}(g)\subseteq B_{d_j}(f^*_g)$ for \emph{all} $j\geq i_0$. Therefore, we can define 
    \[X_j':=\{f^*_g| g\in X_j^*\}\subseteq X_{i_0}\] 
    for all $j\geq i_0$ to obtain an extension
    $X'=(X_1,...,X_{i_0-1},X_{i_0}',...,X_k'$) of $(X_1,...,X_{i_0-1})=X^{(i_0-1)}$ which satisfies
    \[\bigcup_{f^*_g\in X_i'}B_{d_i}(f^*_g) \supseteq \bigcup_{g\in X_i^*}B_{d_i}(g)=V\]
    and
    \[|X_i'|\leq |X_i^*|\]
    for all $i\in\{1,...,k\}$ and is therefore an optimal solution.
    Moreover, since $X_{i_0}$ is \emph{locally} optimal (as shown above), we have both
    \[ X_{i_0}'\subseteq X_{i_0} \text{ (per definition)}\]
    and
    \[|X_{i_0}| \leq |X_{i_0}'|\] 
    and therefore
    \[X_{i_0}'=X_{i_0}.\] 
    Consequently, $X'$ is in fact an extension of $X^{(i_0)}$, showing that $X^{(i_0)}$ is extensible to an optimal solution, as desired.\\
    In particular, this shows that $X^{(k)}=X$ can be extended to an optimal solution, and therefore is already optimal.
\end{proof}

While we have now shown that \textsc{CFR} on trees is solvable in polynomial time if $\alpha=0$ and $F_1=...=F_k=F$ \footnote{The complexity of tree-instances with 
arbitrary $\alpha\in[0,1]$ and level restrictions will remain an open question for now.}, in the next chapter we will discuss its complexity on general instances.