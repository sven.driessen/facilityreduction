\section{Complexity}
\label{sec:complexity}
In the last sections we introduced the \textsc{CFR} problem and showed that it is solvable in polynomial time when
restricted to tree-instances with no margin of error and no level restrictions. We now want to examine how that
changes with the general CFR problem. As it turns out, we will find that CFR in general is in fact NP-complete.
First, we give a formal definition of the Set Cover problem, which we already mentioned earlier.

\begin{shaded}
    \begin{definition}[Set Cover Problem] \hfill \\
        \textbf{Given:}
             A set of elements $N$, a collection $\mathcal{S}=\{S_1,...,S_m\}$ of subsets $S_j\subseteq N$ with $\bigcup\mathcal{S}=N$. \\
        \textbf{Find:} 
             A \emph{minimal} set $\mathcal{S'}\subseteq\mathcal{S}$ such that $\bigcup\mathcal{S'}=N$ (called a \emph{set cover}).
    \end{definition}
\end{shaded}
We will now show the NP-hardness of \textsc{CFR} by giving a polynomial reduction from Set Cover.
\begin{shaded}
    \begin{lemma}\label{lem:reduction}
        Set Cover is polynomially reducible to CFR.
    \end{lemma}
\end{shaded}
Note that while we formulated the \textsc{CFR} very generally, in most practical applications the instances will be \emph{metric}, i.e. based on some metric space $(X,d)$,
which satisfies the triangle inequality
\[d(x,z) \leq d(x,y)+d(y,z) \text{ for all } x,y,z\in X.\] 
In particular, the instances will most probably be based on some connected graph $G=(V,E,c)$, which induces a metric space $(V,d)$ via the induced metric $d=d_G$ defined by
the lengths of shortest paths. This is exactly the case our original real-world problem of reducing the number of hospitals in Germany which this thesis is based on.
Now even if we show that the more general \textsc{CFR} problem is NP-hard, it might still be that the more relevant case where the instances are assumed to be metric is
actually solvable in polynomial time. For this reason, we give a reduction from Set Cover to the \emph{metric} version of \textsc{CFR}, which is a much stronger
result which implies the weaker one.
\begin{proof}
    Let $I=(N,\mathcal{S}=\{S_1,...,S_m\}$) be an instance of Set Cover.
    We define a corresponding \emph{metric} \textsc{CFR} instance $I'$ as follows.
    We first define an undirected, weightless (i.e. with constant edge weights of 1) graph $G=(V,E)$ which $I'$ will be based on. We define
    \[V:=N\cup\{r\}\cup S\] 
    where $S:=\{s_1,...,s_m\}$ (each $s_j$ representing the corresponding set $S_j\in\mathcal{S}$) and $r$ is an auxiliary root that will ensure the connectivity of the graph. \\
    We then define the edges
    \[E:=\{\{s_j,n\}|\ s_j\in S, n\in N, n\in S_j\} \cup \{\{s_j,r\}| s_j\in S\},\] 
    i.e. we connect $n$ and $s_j$ if and only if $n$ is covered by the corresponding set $S_j\in\mathcal{S}$, and to ensure that $G$ is connected we connect
    each $s_j$ to the auxiliary root vertex $r$. Note that because $\bigcup_{S_j\in \mathcal{S}}S_j=N$ every $n\in N$ will be connected to at least on $s_j\in S$.
    Since $G$ is connected, we obtain the induced metric 
    \[d_G:V\times V\longrightarrow \R_{\geq 0},\ d_G(u,v) = |p_{[u,v]}|\] 
    where $|p_{[u,v]}|$ denotes the length of any shortest path between $u$ and $v$ (which is well-defined, since the length of shortest paths is unique per definition). \\
    Using this, we can now define our instance $I'=(V,F,d,F_1,d_1,\alpha)$ by setting $V:=V(G), F:=S\cup{r}, d:=d_G, F_1=F, d_1=1$ and $\alpha=0$. See Figure~\ref{fig:reduction}
    for a graphical representation of this reduction.\\
    From this definition it directly follows that for all $s_j\in F$, 
        \[B_{d_1}(s_j)=\{v\in V| d(n,s_j)\leq d_1\} = S_j\cup\{s_j,r\}.\]
    For a solution candidate $\mathcal{S}'\subseteq\mathcal{S}$ of $I$, we define the corresponding solution candidate 
        $X_{\mathcal{S}'}=\{s_j|S_j\in\mathcal{S}'\}\cup\{r\}$ of $I'$.
    Then for any $\mathcal{S}'\subseteq\mathcal{S}$, using that $r$ and $S$ are guaranteed to be covered by $r$, we obtain 
        \[\bigcup_{S_j\in \mathcal{S}'} S_j=\mathcal{S} \Leftrightarrow \text{ for all $v\in V$, there exists an $s_j\in X_{\mathcal{S}'}$ with $d(n,s_j)\leq d_1.$}\]
    Furthermore, $\mathcal{S}'$ is minimal if and only if $X_{\mathcal{S}'}$ is minimal.
    Therefore, we have an equivalency of solutions for $I$ and $I'$, as desired. 
\end{proof}
\input{tikz/tikz_reduction}
Before we continue, we need to introduce the decision variant of our problem.
\begin{shaded}
    \begin{definition}[$k-$Constrained Facility Reduction Problem] \hfill \\
        \textbf{Given:} A \textsc{CFR}-instance $I$, $k\in\N$. \\
        \textbf{Problem:} Decide if there exists a solution $X$ of $I$ with $|X|=k$.
    \end{definition}
\end{shaded}
Now we can finally show that CFR is indeed NP-complete.

\begin{shaded}
    \begin{theorem}\label{thm:complexity}
        Constrained Facility Reduction is NP-complete.
    \end{theorem}
\end{shaded}
\begin{proof}
    \textbf{(i)} \textsc{CFR}$\ \in\NP$: \\
    It suffices to show that the decision variant of \textsc{CFR} is in \NP,
    since the optimization variant can then be solved with quadratic overhead by iteratively solving the decision variants for increasing $k\in\N$.
    Let $k\in\N$ and let $I=(V,F,d,(F_1,...,F_k),(d_1,...,d_k),\alpha)$ be an instance of \textsc{CFR}. 
    A nondeterministic Turing Machine is able to guess both a solution candidate $X$ as well as a subset $V'\subseteq V$ with $|V'|\geq(1-\alpha)|V|$.
    Given these, verifying if $X$ satisfies the reachability requirement on $V'$ can be done in time $O(|V'||X|)\subseteq O(|V|^2)$. 
    Therefore, \textsc{CFR}$\ \in\NP$. \\
    \textbf{(ii)} \textsc{CFR} is NP-hard: \\
    This directly follows from the NP-hardness of Set Cover (\cite{karp1972reducibility}) and Lemma~\ref{lem:reduction}.
\end{proof}

With Theorem~\ref{thm:complexity} we now know that there is no polynomial-time algorithm solving our problem, not even for metric instances (as
shown in the proof of Lemma~\ref{lem:reduction}.)
Therefore, unless we know that our instances will have a special structure like in the previous section on trees, 
our only hope of finding good solutions for is to use heuristic algorithms, which will be the topic of Section~\ref{sec:algorithms}.
Before this, however, in the next section we will formulate our problem as an Integer Program, 
allowing us to use LP-relaxation to find a lower bound to the optimal value in polynomial time.