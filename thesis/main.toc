\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}Background}{3}{section.2}%
\contentsline {section}{\numberline {3}Problem Formulation}{4}{section.3}%
\contentsline {section}{\numberline {4}Mathematical Model}{4}{section.4}%
\contentsline {subsection}{\numberline {4.1}Mathematical Background}{7}{subsection.4.1}%
\contentsline {section}{\numberline {5}Trees}{9}{section.5}%
\contentsline {section}{\numberline {6}Complexity}{14}{section.6}%
\contentsline {section}{\numberline {7}Integer Program}{17}{section.7}%
\contentsline {section}{\numberline {8}Instance Optimization}{20}{section.8}%
\contentsline {paragraph}{Dominating Facilities and Locations}{21}{section*.6}%
\contentsline {section}{\numberline {9}Algorithms}{24}{section.9}%
\contentsline {subsection}{\numberline {9.1}Set Cover and R-CFR}{25}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Margin of Error}{27}{subsection.9.2}%
\contentsline {paragraph}{Static Reduction.}{28}{section*.9}%
\contentsline {paragraph}{Greedy Selection}{30}{section*.10}%
\contentsline {subsection}{\numberline {9.3}Final Solution Heuristics}{30}{subsection.9.3}%
\contentsline {paragraph}{Essential Facilities.}{30}{section*.11}%
\contentsline {paragraph}{Multiple Levels.}{31}{section*.12}%
\contentsline {section}{\numberline {10}Evaluation}{36}{section.10}%
\contentsline {subsection}{\numberline {10.1}Instance Generation}{36}{subsection.10.1}%
\contentsline {paragraph}{Number of Levels - $k$.}{37}{section*.18}%
\contentsline {paragraph}{Level Restrictions - $(F_1,...,F_k)$.}{37}{section*.19}%
\contentsline {subsection}{\numberline {10.2}Algorithms}{39}{subsection.10.2}%
\contentsline {subsection}{\numberline {10.3}Results}{40}{subsection.10.3}%
\contentsline {subsubsection}{\numberline {10.3.1}Static Reduction Heuristics}{40}{subsubsection.10.3.1}%
\contentsline {subsubsection}{\numberline {10.3.2}Instance Optimization}{44}{subsubsection.10.3.2}%
\contentsline {subsubsection}{\numberline {10.3.3}Essential Facilities}{47}{subsubsection.10.3.3}%
\contentsline {subsubsection}{\numberline {10.3.4}Solution Heuristics}{48}{subsubsection.10.3.4}%
\contentsline {subsubsection}{\numberline {10.3.5}Conclusion and Example}{50}{subsubsection.10.3.5}%
\contentsline {section}{\numberline {11}Conclusion and Outlook}{54}{section.11}%
\contentsline {section}{\numberline {12}Appendix I: Instance Generation}{56}{section.12}%
\contentsline {subsection}{\numberline {12.1}Locations}{56}{subsection.12.1}%
\contentsline {subsection}{\numberline {12.2}Hospitals}{58}{subsection.12.2}%
\contentsline {subsection}{\numberline {12.3}Distances}{59}{subsection.12.3}%
