
\section{Instance Optimization}
\label{sec:instanceopt}

Before we apply any algorithms to a CFR instance, we want to make sure that is does not contain any unnecessary redundancies that bloat 
the instance while not having any effect on the optimal solution. To this end, we can apply a number of preprocessing optimizations that 
reduce the size of our instance and make it easier to solve. However, the amount of optimizations that we can perform depends heavily on
the allowed margin of error. In the general case $\alpha > 0$, at the beginning we do not yet know which locations have to be covered. We
will call these instances \emph{dynamic}, or instances with \emph{dynamic locations}.
If $\alpha=0$, we have a lot more knowledge that we can work with, since we already know that \emph{every}
location has to be covered. We call these instances \emph{static}, or instances with \emph{static locations}. Observe that we can always 
reduce a dynamic instance to a static one
by heuristically pre-calculating a \emph{fixed} reduced location set $V'$ with $p(V')\geq (1-\alpha)p(V)$ and deleting the remaining vertices.  
While this does allow for more preprocessing optimizations,
it might negatively affect the solution quality, since it forces the same set of locations to be covered on each level and it 
prevents us from adaptively constructing those locations while solving the problem. If time resources are not a problem, it might well be 
more desirable to keep working with the less optimized dynamic instance.

Since both approaches come with both advantages and disadvantages, we will consider both of them, and compare their resulting performance
and solution quality at the end. We begin by defining \emph{dominating} facilities and locations.


\paragraph{Dominating Facilities and Locations}
We say that a facility $f$ is \emph{dominated} by a facility $f'$, denoted by $f\leq f'$, if $B_{d_i}(f)\subseteq B_{d_i}(f')$ 
for all $i\in\{1,...,k\}$ with $f\in F_i$.\\
Furthermore, we say that a location $v$ is \emph{dominated} by a location $w$, denoted by $v\leq w$, if $F_i(v)\supseteq F_i(w)$ 
for all $i\in\{1,...,k\}$.\\
While domination itself is reflexive, we say that a facility $f$ is \emph{dominated} if there exists a facility $f'\not=f$ with $f\leq f'$. 
Otherwise, we say that $f$ is \emph{non-dominated}. Dominated and non-dominated locations are defined analogously. \\

Using the notion of domination, we can remove redundancies from both the set of facilities and the set of locations.
\\
Concerning the facilities, we make the important observation that if a solution $X$ contains a facility $f$ that is dominated by another 
facility $f'\not= f$, 
we can safely replace $f$ by $f'$, which can only improve the solution. Therefore, we only care about nondominated facilities and 
can remove all dominated facilities from our instance. This works for all instances.
Note, however, that there may exist facilities $f_1\neq f_2$ with $f_1\leq f_2$, $f_2\leq f_1$ and $f_1,f_2\not\leq f$ for any other $f\in F$. 
In this case, neither $f_1$ nor $f_2$ are nondominated, because they dominate each other. However, as soon as we eliminate one, the other becomes 
non-dominated. Therefore, we have to \emph{iteratively} eliminate dominated facilities until we are left with a set of nondominated facilities. Therefore, by
\emph{nondominated facilities} we will always refer to this set of remaining facilities.\\
Also note that we \emph{cannot} use a local variant of domination which only depends on one level $i$,
because while exluding facilities that are only dominated on one level does indeed not negatively affect the partial solution for that level,
it \emph{could} negatively affect the solutions on higher levels, where the removed facilities might be non-dominated.
\\

A similar observation can be made for dominated locations (however we will see that it only works for static instances.)
Covering a location $w$ automatically guarantees the coverage
of all dominated locations $v\leq w$. The only ``essential'' locations are therefore the \emph{nondominated} locations, and they can be 
interpreted as \emph{representatives} for all locations that are dominated by them, as they ensure the coverage of all the locations they 
dominate, or \emph{represent}. 
As a result, all reachability constraints 
including dominated locations are redundant, and
these locations can safely be removed. Again, as with nondominated facilities, with \emph{nondominated locations} we always refer to the set of locations 
left after \emph{iteratively} eliminating dominated locations. The resulting reduced location set
is potentially a lot smaller than the original set $V$, and contains only those locations that provide non-redundant constraints
to the problem. Therefore, if $\alpha=0$, the resulting reduced instance is still equivalent to the original instance, since no constraints were 
removed or changed. \\
 However, this \emph{only} works for static instances, i.e. if $\alpha = 0$. If $\alpha>0$,
then it may be that an optimal solution excludes a dominating location $w\in V$ from some $V_i'$, thereby possibly turning previously dominated 
locations $v\leq w$ into nondominated locations in $V_i'$. In this case, we cannot simply remove dominated locations. Therefore, we have to find 
another solution. \\

If we have a dynamic instance, pre-processing the locations becomes harder, as the redundancy of their corresponding reachability 
constraints depends heavily on the yet unknown choices of the covered locations $V_i'$. Therefore, deleting dominated locations will 
generally \emph{not} result in an 
equivalent instance. Our goal is now to find another relation on the set of locations that is invariant under the operation of location reduction.
Luckily, we do not need to change much. While domination, which is an ordering relation, does not suffice here, turning it into 
an equivalency relation resolves our problem. We introduce the notion of cover-equivalency.
\paragraph*{Cover-Equivalency.}

We say that two locations $v,w\in V$ are \emph{cover-equivalent}, denoted by $v\sim w$, if $F_i(v)=F_i(w)$ for all $i\in\{1,...,k\}$, where
$F_i(v):=\{f\in F_i| v \in B_{d_i}(f)\}$ is the set of facilities covering $v$.
Note that $v\sim w$ if and only if $v\leq w$ and $w \leq v$. Since $\sim$ is an equivalency relation on $V$, we can construct the 
quotient set $V/_{\sim}=\{[v]|v\in V\}$, where $[v]:=\{w| v\sim w\}$ denotes the equivalency class of $v\in V$, which we will also refer to 
as a \emph{coverage class}.
Intuitively, each coverage class represents a set of locations that are indistinguishable by the facilities. 
Cover-equivalency has the important property that if $f$ covers $v$ and $v\sim w$, then $f$ covers $w$. That is, cover-equivalency is 
\emph{coverage-invariant}, justifying its name. Using this property, we can select one representative location $r=r([v])\in [v]$ from each 
coverage class $[v]\in V/_{\sim}$ and delete the remaining locations $[v]\setminus\{r\}$, resulting in the reduced set 
$V_{\sim}=r(V/_{\sim})=\{r([v])|[v]\in V/_{\sim}\}$, which we will also call the \emph{coverage-reduction} of $V$ (or of the instance).
We only need to keep track of the population of each class by
setting $p(r([v])):=p([v])=\sum_{w\in [v]}p(w)$, which ensures $p(V/_{\sim})=p(V)$. \\

An example of coverage classes and nondominated locations is shown in Figure~\ref{fig:tikz_locations}.

\begin{figure}[h!]
    \centering
    \input{tikz/tikz_location_opt.tex}
    \caption{Example instance with locations $V=\{v_1,...,v_6\}$, facilities $F=\{f_1,f_2\}$, $k=1$ and $\alpha=0$.\\ Here, the coverage classes are
    $V/_{\sim}=\{\{v_1,v_2\},\{v_3,v_4\},\{v_5,v_6\}\}$, and a possible set of representatives is $V_{\sim}=\{v_1,v_3,v_5\}$.
    A possible set of non-dominated locations is $\{v_1,v_5\}$.}
    \label{fig:tikz_locations}
\end{figure}

Still, the question remains how coverage classes behave with the margin of error, which was the point where domination failed.
The following theorem shows that for any instance, the reduction to $V_{\sim}$ is solution-equivalent.
\begin{shaded}
    \begin{theorem}
        Let $X^*$ be an optimal solution for the reduced location set $V_{\sim}$. Then $X^*$ is also an optimal solution for the original 
        instance.
    \end{theorem}
\end{shaded}
\begin{proof}
Let $X^*$ be an optimal solution for the reduced location set $V_{\sim}$. Let $V_i':=\bigcup_{f\in X_i^*}B_{d_i}(f)$ be 
the set of locations in $V$ that are covered by $X^*$ on level $i$ and let $V'_{\sim,i}:=V_i'\cap V_{\sim}$ be the representative locations 
in $V_{\sim}$ that are covered by $X^*$ on level $i$.
Since $X^*$ is an optimal solution, in particular it must hold that
\[ p(V'_{\sim,i})\geq (1-\alpha)p(V_{\sim})=(1-\alpha)p(V) \]
and therefore
\[p(V_i')=\sum_{v\in V_i'}p(v)=\sum_{[v]\in V_i'/_{\sim}}\sum_{w\in [v]}p(w)=\sum_{r\in V'_{\sim,i}}p(r)=p(V'_{\sim,i})\geq (1-\alpha)p(V_{\sim}).\]
where we used in the second equality that if $v\in V_i'$, then $[v]\subseteq V_i'$ and therefore $[v]\in V_i'/_{\sim}$ due to the cover-invariance of $\sim$.
Therefore, $X^*$ is a feasible solution for the original instance.
On the other hand, if we have any feasible solution $X$ on V, then $X$ is also a feasible solution on $V_{\sim}$ 
(as we only remove restrictions). So any optimal solution $X$ on $V$ with $|X|<|X^*|$ would also be a better solution on $V_{\sim}$,
contradicting its optimality. Therefore, $X^*$ is already an optimal solution on $V$.
\end{proof}

This theorem tells us that the solution to the problem only depends on the coverage classes. Locations inside the same class do not carry any different
information. This is true for every instance, and therefore we can safely reduce any instance to $V_{\sim}$ before performing any other work on it.
Also note that this reduction negates the size overhead we might get from starting with an instance that uses a high number of locations as an approximation 
of the real world geometry.

In conclusion, we obtain two sets of optimizations we can perform, the ``full'' optimization including the elimination of dominated facilities and locations, which 
is only applicable for static instances, and the ``reduced'' optimization including the elimination of nondominated facilities and the coverage-reduction, which 
is also applicable for dynamic instances.

The result is an equivalent optimized instance that is potentially much smaller than the original instance, and will be the base for the following
sections in which we will develop heuristics for efficiently computing a good solution.
