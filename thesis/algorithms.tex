\section{Algorithms}\label{sec:algorithms}
In this section, we will develop heuristic algorithms that hopefully allow us to find good solutions to the Constrained Facility Reduction
problem in a reasonable time. The ideas will largely be based on the observation that CFR is, in its roots, equivalent to a ``simple''
covering problem. We have already discussed how CFR can be interpreted as a covering problem in section~\ref{sec:model} and we have 
given a reduction from Set Cover to CFR in section~\ref{sec:complexity}. In particular, we have given a reduction to a the subclass
of CFR where $k=1$ and $\alpha=0$, which we will from now on call \emph{R-CFR} or \emph{Root-CFR}.
We will now show that R-CFR and Set Cover are computationally equivalent by additionally giving a polynomial reduction from R-CFR to Set Cover.
\begin{shaded}
    \begin{lemma}
        R-CFR is polynomially reducible to Set Cover in time $O(|V|\cdot|F|)$.
    \end{lemma}
\end{shaded}
\begin{proof}
    Let $I=(V,F,d,d_1)$ (where $F_1=F$ and $\alpha=0$ are left out) be an R-CFR instance. Then we define the corresponding 
    Set Cover instance $I':=(N,\mathcal{S})$ where $N:=V$ and $\mathcal{S}=\{B_{d_1}(f)|f\in F\}.$
    For any solution $X\subseteq F$, we define the corresponding solution $\mathcal{S}_X:=\{B_{d_1}(f)|f\in X\}$.
    Then it holds that $\max_{v\in V}\min_{f\in X}d(f,v)\leq d_1$ \emph{if and only if} $\bigcup\mathcal{S}_X = V$, and
    therefore $X$ is an optimal solution for $I$ if and only if $\mathcal{S}_X$ is an optimal solution for $I'$.
    Finally, the computation of each set $B_{d_1}(f)$ requires $|V|$ steps, and this computation has to be done $|F|$ times,
    so the $I'$ can be computed in time $O(|V|\cdot |F|)$.
\end{proof}

In consequence, a good starting point for finding good heuristics for CFR is to apply already known algorithms for the well-researched
Set Cover problem and convert them into algorithms for R-CFR. We can then start extending these algorithms by including the margin of error
$\alpha$ and multiple facility levels.

\subsection{Set Cover and R-CFR}
For Set Cover, there exists a well known greedy algorithm with an approximation ratio of $H(|S_{\max}|)\leq ln(n)$ 
\cite{chvatal1979greedy}, where $H(n)$ denotes 
the $n$-th harmonic number defined by $H(n)=\sum_{i=1}^n\frac{1}{i}$ and $S_{\max}$ denotes the set $S\in\mathcal{S}$ with maximum cardinality.
Dinur and Steurer (2013) showed that Set Cover cannot be approximated to a factor of $(1-o(1))ln(n)$ unless $P=NP$ \cite{dinur2014analytical}.
Therefore, assuming $P\not=NP$, the greedy algorithm has an optimal approximation ratio. We will now formulate the algorithm, modified to fit
R-CFR instances. The idea of the algorithm is simple: In each iteration, we choose the facility that covers the most currently uncovered
locations and add it to our solution. We stop when all locations are covered.

\begin{shaded}
    \begin{algorithm}[H]\label{alg:RCFR}
        \caption{Greedy Algorithm for R-CFR}
        \textbf{Input:} An \textsc{R-CFR} instance $I=(V,F,d,d_1)$\\
        \textbf{Output:} A solution $X$ to $I$ if one exists, and \emph{failure} otherwise.
        \begin{algorithmic}[1]
            \If{$\bigcup_{f\in F}B_{d_1}(f) \not= V$} \Return \emph{failure} \EndIf 
            \State $U \gets V, F' \gets F, X\gets \emptyset$
            \While{$U\not=\emptyset$}
                \State $f^* \gets \argmax_{f\in F'}|B_{d_1}(f)\cap U|$
                \State $X\gets X\cup\{f^*\}, F'\gets F'\setminus\{f^*\}, U\gets U\setminus B_{d_1}(f^*)$
            \EndWhile
            \State \Return $X$
        \end{algorithmic}
    \end{algorithm}
\end{shaded}

The algorithm can be implemented in time $O(|F|\cdot |V|)$. Because Set Cover is polynomially reducible to R-CFR, we can infer that
R-CFR can also have no algorithm with a better approximation ratio than $(1-o(1))ln(n)$. Since the algorithm essentially corresponds to 
the greedy algorithm for Set Cover applied to a reduced R-CFR instance, we can expect that its approximation factor is also optimal. 
Nevertheless, we will now give a proof for this important property.

\begin{shaded}
    \begin{lemma}
        \label{lem:approx}
        Algorithm~2 has an approximation ratio of $H(s_{\max})$, where 
        $H(n)$ denotes the $n$-th harmonic number defined by $H(n)=\sum_{i=1}^n\frac{1}{i}$ and $s_{\max}=\max_{f\in F}|B_{d_1}(f)|$.
    \end{lemma}
\end{shaded}
\begin{proof}
    Let $X^*$ be an optimal solution and let $X$ be the solution computed by the algorithm. Whenever any $f\in F$ is added to $X$ in the algorithm, we can distribute the cost of $f$
    (which is 1) over the new locations it covers. For any $v\in V$, let $f$ be the facility that first covers $v$ in the algorithm and define 
     $c(v)=\frac{1}{|V_f|}$ where $V_f:=B_{d_1}(f)\cap V'$ and $V'$ refers to the currently uncovered locations before $f$ is added, as in the algorithm.
     Note that $V_f\cap V_{f'}=\emptyset$ for all $f\not=f'$, and therefore $V=\dot{\bigcup}_{f\in X}V_f$.
    Then it holds that 
        \[|X| = \sum_{f\in X}1 = \sum_{f\in X}\sum_{v\in V_f}c(v) = \sum_{v\in V} c(v)\]
    For any $f^*\in X^*$, we order the locations in $B_{d_1}(f^*)=\{v_s,...,v_1\}$ in \emph{descending order of their covering} 
    in the algorithm, i.e. for $l\in\{1,...,s-1\}$, $v_l$ is covered \emph{before} $v_{l+1}$.
    Then for any $l\in\{1,...,s\}$, right before $v_l$ is covered in the algorithm by some facility $f$, 
    there are at least $l$ locations in $B_{d_1}(f^*)$ that are yet uncovered, namely $v_1,...,v_l$. Therefore, $f^*$ would cover at 
    least $l$ new locations, and since we chose $f$ over $f^*$ in the algorithm, $f$ has to cover \emph{at least as many} new locations
    as $f^*$. Thus we obtain $|V_f|\geq l$, yielding
    \[c(v_l)=\frac{1}{|V_f|}\leq \frac{1}{l}\]
    and Therefore
    \[c(B_{d_1}(f^*))=\sum_{l=1}^s c(v_l) \leq \sum_{l=1}^s \frac{1}{s} = H(s) \leq H(s_{\max}).\]
    Since each $v\in V$ is covered by at least one $f^*\in X^*$, we can conclude
    \[|X|=\sum_{v\in V}c(v)\leq \sum_{f^*\in X^*}\sum_{v\in B_{d_1}(f^*)}c(v)\leq\sum_{f^*\in X^*}c(B_{d_1}(f^*))\leq |X^*|\cdot H(s_{\max})\]
    as desired.

\end{proof}

In the following sections, we will expand algorithm 2 to handle all the extra elements remaining in the general CFR problem.

\subsection{Margin of Error}
We will now develop two methods of incorporating the margin of error $\alpha$ into our greedy algorithm. If our instance is dynamic, only a 
reduced set $V_i'$ of locations has to be covered on each level $i$.
There are two approaches we can follow: Either we precompute these sets of locations using some heuristic, or we dynamically construct in the solution heuristics.
The advantage of the first approach is that it allows us to transform our instance into a static instance, which we call \emph{static reduction}. However, this 
requires the computation of a \emph{single} set $V'$ of locations that need to be covered on each level, so that we can obtain a new instance $I'$ with the new 
location set $V'$ and $\alpha=0$. Remember that we required the existence of such a set $V'$ in the definition of \textsc{CFR} in Section~\ref{sec:model}. 
The static reduction allows us to make use of the improved instance optimizations and will provide further advantages in the solution heuristics. Note that 
a more optimized (and thus smaller) instance not only improves performance, but may also result in a better solution value according to the approximation ratio
of $H(s_max)$ which we have proven for \textsc{R-CFR}, as less locations result in smaller covers $B_{d_i}(f)$.\\
However, as the static reduction restricts the instance to a single fixed location set $V'$ for all levels, it also severely restricts the solutions in their freedom,
possibly resulting in much worse solution values. We consider this approach nontheless, as the higher level of optimization might be a higher priority in some scenarios.

\paragraph{Static Reduction.}
To precompute the set $V'$, the first thing we need to do is to remove all uncovered locations, as each location needs to be covered in the resulting instance.
Assuming that all uncovered locations are removed first, we can then proceed to remove more locations until the margin of error is completely exhausted. 
For this, we need to find a heuristic that predicts which locations provide the largest constraints to the solution, so we can remove them.
One very straightforward heuristic is to remove those locations that are covered by the fewest facilties on some level and are thus harshly restricting the set 
of possible solutions. 
We call such locations \emph{anchors}. In the worst case, an anchor is covered by only a single facility - which is therefore forced to be included in any solution.
We call such facilities \emph{essential facilities}, and we will further discuss them later.
So with this heuristic, we may potentially greatly reduce the set of essential facilities. Note that the set of facilities $F_i(v)$ covering a location $v$
is dependent on the level $i$. So which level do we choose to make our decision? While it might seem intuitive to prioritize the first level as the objective does,
remember that essential facilities are dependent on higher levels. If a facility is essential for some level $i$, then it needs to be included in the partial solutions
for \emph{all} levels $i'\leq i$, including $i'=1$. Hence it makes much more sense to prioritize facilities that are essential on higher levels, and consequently 
we should delete anchors for higher levels first. We therefore formally define an anchor as follows. 
\\
Let $\delta(V):=\min_{v\in V, i\in\{1,...,k\}}|F_i(v)|$.
Then a location $v$ is called an $i-anchor$ if $|F_i(v)|=\delta(V)$ and $i$ is maximal with this property.
\\
Now that we have defined what an anchor is, we could already identify and remove them. But remember that we have a limit on the number of locations we are allowed 
to remove, depending on their population. Therefore, to maximize the number of locations we can remove, we prioritize anchors with a low population. We can now
formulate an algorithm implementing these ideas.


\begin{shaded}
    \begin{algorithm}[H]
        \caption{Anchor Deletion}
        \textbf{Input:} A CFR instance $I=(V,F,d,p,(F_1,...,F_k),(d_1,...,d_k),\alpha)$\\
        \textbf{Output:} A reduced location set $V'$ with $p(V')\geq(1-\alpha)p(V)$ \\
        \begin{algorithmic}[1]
            \State $V'\gets V$
            \State $\varOmega \gets \{(v,i)|v\in V, i\in\{1,...,k\}, p(v)\leq p(V')-(1-\alpha)p(V)\}$
            \State Order $\varOmega$ lexicographically in ascending order using the key $k(v,i)=(|F_i(v)|,-i,p(v))$
            \While{$\varOmega\not=\emptyset$}
                \State $(v^*,i^*)\gets\mathrm{head}(\varOmega)$
                \State $V'\gets V'\setminus\{v^*\}$
                \State delete $(v,i)\in\varOmega$ with $v=v^*$ or $p(v)\leq p(V')-(1-\alpha)p(V)$
            \EndWhile
            \State \Return $V'$
        \end{algorithmic}
    \end{algorithm}
\end{shaded}

Precalculating $|F_i(v)|$ for all $i\in\{1,...,k\}$ and $v\in V$ can be done in time $O(k\cdot m\cdot n)$. Then the time for sorting $\Omega$ lies
in $O(k\cdot n\cdot \log(k\cdot n))$. The while-loop terminates after at most $k\cdot n$ iterations and can be implemented in time $O(k\cdot n)$.
In total, we obtain a running time in $O(k\cdot n\cdot(m+\log(k\cdot n)))$ for Anchor Deletion. \\

Another approach would be to focus completely on removing the largest amount of locations, thereby minimizing the total area that has to be covered.
This heuristic is much simpler than the previous one, since we only need to remove the locations with the lowest population until the limit is reached.

\begin{shaded}
    \begin{algorithm}[H]
        \caption{Area Sweep}
        \textbf{Input:} A CFR instance $I=(V,F,d,p,(F_1,...,F_k),(d_1,...,d_k),\alpha)$\\
        \textbf{Output:} A reduced locations set $V'$ with $p(V')\geq(1-\alpha)p(V)$ \\
        \begin{algorithmic}[1]
            \State $V'\gets V$
            \State Sort $V'$ in ascending order using the key $k(v)=p(v)$
            \While{$p(\mathrm{head}(V'))\leq p(V')-(1-\alpha)p(V)$}
                \State $\mathrm{pop}(V')$
            \EndWhile
            \State \Return $V'$
        \end{algorithmic}
    \end{algorithm}
\end{shaded}

Area Sweep can be implemented in time $O(n\cdot \log(n))$. \\

Once $V'$ is computed, we obtain a static instance $I'$ by replacing $V$ by $V'$ and setting $\alpha=0$. As discussed in section~\ref{sec:instanceopt}, we
can now apply further optimizations to $I'$ by deleting dominated locations. Additionally, we will be able to precompute essential facilities
as a headstart for the solution heuristics.

\paragraph{Greedy Selection}
If we decide against precomputing $V'$, we can incorporate the selection of locations into our greedy algorithm. In the case $k=1$, we can
build upon the greedy algorithm for R-CFR by simply stopping as soon as \emph{enough} locations are covered, instead of when \emph{all} locations are covered.
That is, we simply stop the iteration as soon as $p(U)\leq\alpha p(V)$ instead of $U=\emptyset$. Intuitively, this way we prioritize those locations that 
are coverable by ``good'' facilities, i.e. facilities that cover as many new locations as possible. Note that this cannot be precomputed, since it depends on 
the facilities that we choose throughout the greedy algorithm. \\
For $k>1$ we can do something similar, as we will see in the next section where we incorporate multiple levels into our algorithm. This will result 
in our final solution heuristics.

\subsection{Final Solution Heuristics}

The only thing left to do is to incorporate multiple levels into our greedy heuristic. But first, \emph{if our instance is static}, for example if we 
decided to use a static reduction, we can give ourselves a 
headstart by identifying 
\emph{essential facilities}, which will be guaranteed to be included in any solution.

\paragraph{Essential Facilities.}
We say that a facility $f\in F$ is 
\emph{essential for level $i\in\{1,...,k\}$} or \emph{i-essential}
if for any $j\geq i$ there exists a location $v\in B_{d_{j}}(f)$ with $F_{j}(v)=\{f\}$. We may just call a facility \emph{essential} if it is $i$-essential for some $i$.
If our instance is static, i.e. $\alpha=0$, then every location needs to be covered, so we already know that any partial solution $X_i$ needs 
to contain \emph{all} $i$-essential facilities. Therefore, before applying any heuristics we can identify all essential facilities and add them
to the solution, giving any subsequent heuristics a headstart. This of course only works for the case $\alpha=0$, because otherwise we do not know 
at the beginning 
which locations will need to be covered and which do not. Calculating the essential facilities can be done in time $O(k\cdot m\cdot n)$. \\

\paragraph{Multiple Levels.}
Incorporating multiple levels into our algorithm proves to be a hard problem. The difficulty arises mainly due to the fact that on higher levels,
we may have a restricted selection of facilities, which may lead to the problem that the facilities contained a partial solution $X_i$ may not be 
available on level $i+1$, i.e. $X_{i}\cap F_{i+1}\subsetneq X_{i}$, and thus we cannot ensure that sufficiently many locations are coverable on 
level $i+1$.
Hence, while computing $X_i$ we simultaniously need to make sure to maintain sufficient coverability on all remaining levels $j>i$, i.e. we need 
to ensure 
\emph{higher level coverability}. The problem is that the optimal coverage of the current level and higher level coverability are two adversarial 
goals, in the sense that higher level coverability is necessary but achieving it might negatively affect the quality of the partial solution on the
current level. We present three approaches for incorporating higher level coverability into our algorithm, all prioritizing different aspects.
\\

First, we could calculate the partial solutions in reversed order, starting with $X_k$, 
then extending $X_k$ to $X_{k-1}$, and so on, until we reach $X_1$. We call this the \emph{top-down} approach.
This approach would be easy and elegant, and it definitely assures coverage on all levels, but unfortunately it prioritizes 
higher levels over lower levels, which is the opposite of what our objective requires. 
\\

Second, we could use the opposite approach and prioritize optimizing the coverage on the current level, and only then recover higher level coverability.
More precisely, on level $i$ we would first calculate the partial solution $X_i$ as per our greedy heuristic, always selecting the facility $f\in F$ that 
covers the most uncovered locations on level $i$, until we have achieved full coverage on level $i$. Then, if any higher level $j>i$ is not yet coverable,
we add new facilities $f\in F_j$ (also according to our greedy heuristic, but for level $j$) to $X_i$ until coverability on level $j$ is ensured. We do this
until higher level coverability is completely ensured, and then move on to the next level. We call this approach the \emph{bottom-up approach}.
\\

These first two approaches have both advantages and disadvantages. Let us compare the quality of the partial solution $X_i$ for some $i\in\{1,...,k\}$.
The top-down approaches minimizes the number of facilities in $X_i$ needed for ensuring higher level coverability, but in turn these facilities 
might be very badly suited for the coverage on level $i$, thereby not contributing much to the solution quality of $X_i$ and requiring more additional 
facilities to ensure coverage on this level. On the other hand, the bottom-up approach minimizes the number of facilities required for coverage on level $i$,
but these might be ill-suited for providing higher-level coverability, thus requiring more additional facilities to ensure that. 
\\

Both approaches have completely contrary focuses. The first prioritizes optimal coverage, the second prioritizes higher level coverability, both of which
may affect the solution quality depending on the underlying instance. We introduce a third approach, called the \emph{balanced approach}, that aims to combine 
and balance both of these aspects. This approach consists of two layers, the outer layer having a bottom-up structure and the inner layer having a top-down 
structure. We iteratively compute solutions for the levels $1,...,k$ but in each iteration we perform a top-down computation that aims to select good 
facilities for level $i$ while simultaniously ensuring higher level coverability. More precisely, when computing the partial solution $X_i$, we start by 
selecting facilities $f\in F_k$ until level $k$ is covered, then we select facilities $f\in F_{k-1}$ until level $k-1$ is covered, and so on, until we 
have covered the current level $i$. But in contrast to the regular top-down approach, we always select facilities that are \emph{optimal for level $i$}.
From another viewpoint, this can also be seen as performing the regular greedy heuristic for level $i$, but restricting ourselves to facilities $f\in F_j$
for $j\geq i$ until level $j$ is covered by $X_i$. Only then do we loosen the restriction to a larger set of facilities, until we eventually reach $F_i$,
at which point higher level coverage is ensured.
\\
This third approach may have all the advantages and disadvantages of the previous approaches, depending on the instance.
We might solve the problem of the top-down approach that the facilities we select for covering level $j>i$ might be ill-suited for level $i$, but they 
might still not be optimal for level $i$ since there might be much better ones in $F_i\setminus F_j$, and since we do not choose facilities that are 
optimal for level $j$, we might have to include many of them until level $j$ is completely covered - reminiscent of the main problem of the bottom-up approach.
\\
Also note that the bottom-up approach and the balanced approach have the nice property that if $F_1=...=F_k$, we simply perform the greedy heuristic on each
level, thereby always choosing the currently best facility for the current level. This is not true for the top-down approach, which still prioritizes higher 
levels even in this special case. Therefore, this approach should \emph{only} be used for instances where we actually have (harsh) restrictions on the 
sets $F_i$.
\\

So we now have three approaches giving us an outline for how we compute the partial solutions $X_1,...,X_k$ for multiple levels $k>1$ and possibly harsh
restrictions $F_1\supsetneq...\supsetneq F_k$. But we still need to fill in the details of how to handle the margin of error $\alpha$ and how to actually
choose the facilities $f\in X_i$ if the basic approaches leave room for choice.
\\
Handling the margin of error is simple. All of our algorithms will have the same basic structure as the original greedy algorithm for R-CFR: We iteratively
add new facilities to each partial solution and keep track of the currently still uncovered locations. Hence, we can simply stop the iteration as soon as the 
number of uncovered locations is within the margin of error.
\\
In the original greedy algorithm for R-CFR, our only criteria of choice for a ''good`` facilitiy was that is maximizes the number of new, i.e. yet uncovered 
locations. Facilities covering the same amount of uncovered locations were considered equally good. In the multi-level case, we can refine this definition by
incorporating the coverage of higher levels according to the lexicographical priorities of our objective. In fact, we can simply apply the lexicographical 
order to the tuples containing the coverage on all higher levels of each facilitiy, filling in zero for levels on which they are not availbable. 
That is, on level $i$ we lexicographically maximize the function
\[\phi_i(f)=(c_i(f),...,c_k(f))\] 
where the \emph{coverage value} $c_j(f)$ is defined as
\[c_j(f)=\begin{cases} |B_{d_j}(f)\cap U_j|&\text{if }f\in F_j \\ 0&\text{otherwise} \end{cases}\]
with $U_i$ denoting the set of currently uncovered locations on level $i$. Maximizing this function optimizes the objective for the current partial solution $X_i$, 
while ignoring higher level 
coverability. If we want to select facilities from higher levels for the purpose of providing higher level coverability, we instead want to maximize the function
\[\psi_j(f)=(c_j(f),...,c_1(f))\]
where $j$ is generally the maximal level that is yet uncovered. We will now formulate the three algorithms.

\paragraph*{Top-Down Covering.}
The top-down covering algorithm is the easiest, since it only uses $\psi_j$ for the greedy selection.
\begin{shaded}
    \begin{algorithm}[H]
        \caption{Top-Down Covering}
        \textbf{Input:} A CFR instance $I=(V,F,p,d,(F_1,...,F_k),(d_1,...,d_k),\alpha)$. \\
        \textbf{Output:} A solution $X=(X_1,...,X_k)$.
        \begin{algorithmic}[1]
            \State $U_j\gets V$ for all $j\in\{1,...,k\}$
            \For{$j=k,...,1$}
                \While{$p(U_j)>\alpha p(V)$}
                    \State $f^*\gets\argmax_{f\in F_j}\psi_j(f)$
                    \State $X_i\gets X_i\cup\{f^*\}, \ U_i\gets U_i\setminus B_{d_i}(f^*)$ for all $i\leq j$
                \EndWhile
            \EndFor
            \State \Return $X=(X_1,...,X_k)$
        \end{algorithmic}
    \end{algorithm}
\end{shaded}

We can precompute the values $(c_j(f))_{j\in\{1,...,k\},f\in F}$ and update them after each facility added to the solution, which can be done 
in time $O(k*m*n)$ respectively. Selecting $f*$ can be done in $O(k*m)$, and updating $U_i$ in $O(n)$. In total, each iteration of the while-loop 
lies in $O(k*m*n)$. The loop terminates after at most $m$ iterations, as a new facility gets added to the solution in each iteration. With $k$ iterations
of the outer loop, the total running time of the algorithm lies in $O(k^2\cdot m^2\cdot n)$.

\paragraph*{Bottom-Up Covering.}
The bottom-up covering algorithm first optimizes the coverage on the current level using $\phi_i$, and then restores higher level coverability with a minimal
amount of additional facilities using $\psi_j$.
\begin{shaded}
    \begin{algorithm}[H]
        \caption{Bottom-Up Covering}
        \textbf{Input:} A CFR instance $I=(V,F,p,d,(F_1,...,F_k),(d_1,...,d_k),\alpha)$. \\
        \textbf{Output:} A solution $X=(X_1,...,X_k)$.
        \begin{algorithmic}[1]
            \For{$i=1,...,k$}
                \State $U_l\gets V$ for all $l\geq i$
                \While{$p(U_i)>\alpha p(V)$}
                    \State $f^*\gets\argmax_{f\in F_i\cap X_{i-1}}\phi_i(f)$
                    \State $X_i\gets X_i\cup\{f^*\}$
                    \State $U_l\setminus B_{d_l}(f^*)$ for all $l\geq i$ with $f^*\in F_l$
                \EndWhile
                \For{$j=k,...,i+1$}
                    \While{$p(U_j)>\alpha p(V)$}
                        \State $f^*\gets\argmax_{f\in F_j\cap X_{i-1}}\psi_j(f)$
                        \State $X_i\gets X_i\cup\{f^*\}$
                        \State $U_l\setminus B_{d_l}(f^*)$ for all $l\geq i$ with $f^*\in F_l$
                    \EndWhile
                \EndFor
            \EndFor
            \State \Return $X=(X_1,...,X_k)$
        \end{algorithmic}
    \end{algorithm}
\end{shaded}

In comparison to the top-down algorithm, the additional inner for-loop results in an additional factor of $k$ in the running time of the algorithm, 
which therefore lies in $O(k^3\cdot m^2\cdot n)$.

\paragraph*{Balanced Covering.}
Finally, the balanced covering algorithm combines both of the previous two algorithms. Its inner loop structurally resembles the top-down algorithm, but it computes
facilities for the partial solution $X_i$ instead of $X_j$ and uses $\phi_i$ instead 
of $\psi_j$ for the greedy selection, like the bottom-up algorithm.
\begin{shaded}
    \begin{algorithm}[H]
        \caption{Balanced Covering}
        \textbf{Input:} A CFR instance $I=(V,F,p,d,(F_1,...,F_k),(d_1,...,d_k),\alpha)$. \\
        \textbf{Output:} A solution $X=(X_1,...,X_k)$.
        \begin{algorithmic}[1]
            \For{$i=1,...,k$}
                \State $U_j\gets V$ for all $j\geq i$
                \For{$j=k,...,i$}
                    \While{$p(U_j)>\alpha p(V)$}
                        \State $f^*\gets\argmax_{f\in F_j\cap X_{i-1}}\phi_i(f)$
                        \State $X_i\gets X_i\cup\{f^*\}$
                        \State $U_l\gets U_l\setminus B_{d_l}(f^*)$ for all $l\geq i$ with $f^*\in F_l$
                    \EndWhile
                \EndFor
            \EndFor
            \State \Return $X=(X_1,...,X_k)$
        \end{algorithmic}
    \end{algorithm}
\end{shaded}


Similarly to the bottom-up algorithm, we obtain a running time of $O(k^3\cdot m^2\cdot n)$.

\paragraph*{}
In the last few chapters, we have developed a variety of algorithms, including instance optimizations, static reductions and solution heuristics.
In the following chapter, we will implement and evaluate all of these algorithms to determine which combination is best suited for different purposes.