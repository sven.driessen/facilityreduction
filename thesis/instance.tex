\section{Appendix I: Instance Generation} \label{cha:instance} \hfill \\
In this section, we will go through the detailed process of generating the geometry for our instances, using the example state of Nordrhein-Westfalen.
\subsection{Locations} \hfill \\
The locations are based on the "Zensus 2011", a population count of Germany performed in the year 2011 
by the Statistical Offices of Germany (\cite{Zensus}). Precisely, the used dataset consists of a division of the area of Germany
into a grid with 1km$\times$1km cells, each cell containing a population count. The first step is to convert the given
center-coordinates of the grid cells from their original ETRS89-format (a projective coordinate system for Europe) into
the more commonly used Global Coordinate System (GCS), which represents points on the earths surface by their longitude and latitude.
This step is necessary because we will later need the coordinates in this form to calculate the distances between the locations and hospitals.
This transformation is done using the Python library \emph{pyproj}. 
Furthermore, given the center points, the 1km-bounding-box of each cell
had to be computed in GCS-coordinates, which was done using sufficiently accurate approximation-formulas based on 
the World Geodetic System (WGS) that convert longitude and latitude
into their corresponding lenghts on the earths surface. More precisely, given a cell with latitude $\phi$, 
one degree of latitude corresponds to a surface length of
\[111132.92-559.82\cos(2\phi)+1.175\cos(4\phi)-0.0023\cos(6\phi) [m]\]
and one degree of longitude corresponds to a surface length of
\[ 111412.84\cos(\phi)-93.5\cos(3\phi)+0.118\cos(5\phi) [m]. \]


Now, there are two approaches for generating locations from this data.\\

First, we can simply adopt the grid structure of the data by choosing the locations as the center points of the grid cells, labeling them with the
corresponding population. This method captures the population information as accurately as possible, but at the cost of geometric imprecision due to 
the fact that we only choose one representative location per grid cell. In this case, the geometric precision and the number of locations directly
 depend on the grid resolution. We call 
this approach the \emph{population-based} or \emph{grid-based} approach.\\
An alternative approach is to randomly sample a number of points inside each grid cell in order to more accurately represent the whole area of the grid cell.
We could then either distribute the population over the sample points, or get rid of the need for population labels entirely and instead make the number of samples
proportional to the population, which effectively corresponds to a fixed, equal population for all locations. 
This would however lead some imprecision depending on how much population one sample location represents. With this approach, the geometric precision can be 
decided by us, and the number of locations depends not on the grid size anymore, but on the population and the geometric precision. 
We call this approach the \emph{sample-based} or 
\emph{geometric} approach. A comparison of different location sets generated from the same data is shown in Figure~\ref{fig:locations}.

\begin{figure}[h!]
    \includegraphics[width=\textwidth]{img/comparison.png} 
    \caption[comparison]{A comparison of different location generation methods based on the same population data.}
    \label{fig:locations}
\end{figure}

In our case, the grid-based approach is more suitable since the grid resolution is fine enough to allow for sufficient geometric precision, and this way we 
keep the number of locations to a minimum while maintaining accurate population information. However, as we can see in Figure~\ref{fig:locations}, the 
geometric approach is very suitable for visualizing the population distribution, so it will still be useful for us.\\


Lastly, to obtain a division of the entire map of Germany into its states, we need to know which state each of our locations belongs to.
Unfortunately, this information is not included in the original grid cell data, which means that we have to obtain it ourselves.
To this end, we use the free geocoding service \emph{Nominatim}, which uses OpenStreetMap data. Using the reverse geocoding provided by Nominatim,
we can use the coordinates of our locations to query various information on the corresponding geographic location, including its state.
However, the remotely hosted and publically accessible Nominatim server only allows users to make at most one single request per second.
Since we need information on more than 200.000 locations, this would take multiple days of computation - and since Nominatim strongly discourages
large bulk requests to their public server, we should find a different solution. Luckily, Nominatim provides the tools for users to host their
own servers. Therefore, we install a local Nominatim server based on OpenStreetMap data on Germany, which can be freely downloaded from 
\url{http://download.geofabrik.de/europe/germany.html}. Once the server is up and running, we can use the Python library \emph{geopy} to set 
up a geocoder that is able to send requests to our Nominatim server. With this local setup, we can now easily process our bulk-request and receive
the state of all our locations in a reasonable time. Given this new information, we can then easily extract the locations belonging to 
Nordrhein-Westfalen, or any other state.


\subsection{Hospitals}
The hospital locations are based on an list of German hospitals provided by the Statistical Offices of Germany in 2018 (\cite{Hospitals}).
One problem with this list, however, is that the hospital locations are only given in the form of unstructured addresses of varying forms.
Since we need precise locations in the form of GCS coordinates, a translation is necessary. For this, we use the geocoding service provided
by Nominatim, which we already used above to obtain state-information on our locations. But there is a problem: Nominatim provides multiple 
\emph{import styles} when setting up the server based on map-data. The import style basically defines the level of detail needed for the
installation. In our previous case, it suffices to use the \emph{admin} import style, which is the lowest level of detail possible and only 
defines administrative boundaries, including information on the state of a location. This requires very little data in comparison to the 
more detailed import styles, which are node-based. To be able to find the exact coordinates of locations given by their addresses requires
a very high level of detail, down to the street and number of each building in Germany. Setting up a local server with this level of detail 
unfortunately turns out to be impossible on the available computer. Therefore, we have to turn back to the publicly accessible remote server
hosted by nominatim.org. Luckily, while the number of locations lies in the hundreds of thousands,
the number of hospitals in Germany is less than 2000. Thus, even with the limit of one request per second, the coordinates of all hospital
locations can be obtained in a reasonable time. \\
Fortunately, the list of hospitals locations already includes the corresponding state, allowing us to easily extract the hospitals located
in Nordrhein-Westfalen, or any other state. The hospitals in Germany and Nordrhein Westfalen are depicted in Figure~\ref{fig:hospitals}.

\begin{figure}[h!]
    \includegraphics[width=\textwidth]{img/locations_hospitals_Germany+nrw.png} 
    \caption[comparison]{A visualization of the locations and
    hospitals in Germany (left) and in Nordrhein-Westfalen (right).}
    \label{fig:hospitals}
\end{figure}

\subsection{Distances}
Given the set of locations and hospitals for our instance, we finally need to compute the distances between each pair of locations and hospitals.
In this case, the "distance" refers to the travel time by car between two locations on the map. To compute this, we use the \emph{OpenStreetMap Routing Machine (OSRM)},
which lets us perform a variety of routing tasks based on OpenStreetMap-data. Note that for $n$ locations and $m$ hospitals, we 
need to compute $n\cdot m$ distances. Therefore, we will have to make a very large number of requests, even when restricting ourselves to a single state.
As before, this means that we will have to set up our own OSRM-server. As already mentioned, we restrict ourselves to single states - in this case Nordrhein-Westfalen -
to drastically reduce the number of required calculations. Therefore, we download the latest OpenStreetMap data for Nordrhein-Westfalen from geofabrik.de and use that to 
install our server. Here, OSRM gives us a number of options. We use the car-profile, which will calculate distances (or durations) based on travel by car. 
We choose the Contraction Hierarchies algorithm (instead of Multi-Level-Dijkstra, which sacrifices response time for access to additional data at runtime), which
is more efficient for very large requests, as in our case. We set the maximum table size to a sufficiently large number, since the default value 
would not allow the size of our request. Once the server is set up, we use the Python package \emph{python-osrm}, which provides a convenient 
wrapper for most function of the OSRM-API. We then finally calculate our distance table using the \emph{table}-request, setting the locations as sources and the
hospitals as destinations, and setting the return value to travel-time (in minutes) instead of distance.