\section{Heuristics}

\begin{frame}{Heuristics}
    \begin{center}
        $k=1$ and $\alpha=0 \Rightarrow$ \alert{Root-CFR} or \alert{R-CFR}
    \end{center}
    \begin{theorem}
        R-CFR is equivalent to Set Cover.
    \end{theorem}
    $\Rightarrow$ Well-known \alert{optimal} greedy heuristic 
\end{frame}
\begin{frame}{Heuristics}
    \begin{algorithm}[H]
        \caption{Greedy Heuristic for R-CFR}
        \begin{algorithmic}[1]
            \State $U \gets V, X\gets \emptyset$
            \While{$U\not=\emptyset$}
                \State $f^* \gets \argmax_{f\in F}\alert{|B_{d}(f)\cap U|}$
                \State $X\gets X\cup\{f^*\}, U\gets U\setminus B_{d}(f^*)$
            \EndWhile
        \end{algorithmic}
    \end{algorithm}

    \begin{theorem}
        The greedy heuristic for R-CFR has an \alert{optimal} approximation ratio of $H(s_{\max})$, where \\
        \begin{itemize}
            \item $H(n)=\sum_{i=1}^n\frac{1}{i}$ ($n$-th harmonic number)
            \item $s_{\max}=\max_{f\in F}|B_{d}(f)|$
        \end{itemize}
    \end{theorem}
    \begin{itemize}
        \item Depends on the \alert{density} of $V$
            $\Rightarrow$ reduced by instance optimization
        \item Looser \alert{distance restriction} $\rightarrow$ worse approximation ratio
    \end{itemize}
    
\end{frame}

\begin{frame}{Heuristics}
    \begin{center}
        How to handle $\alpha$?
    \end{center}
    \textbf{Terminology:}
    \begin{itemize}
        \item $\alpha>0:$ \alert{dynamic} instance
        \item $\alpha=0:$ \alert{static} instance
    \end{itemize}
    Two approaches for dynamic instances:
    \begin{enumerate}
        \item Perform greedy heuristic \alert{until enough locations are covered}
        \item Transform into a static instance $\Rightarrow$ \alert{static reduction}
    \end{enumerate}
    
\end{frame}

\begin{frame}{Heuristics}
    \begin{center}
        \textbf{Static Reduction}
    \end{center}
    \begin{itemize}
        \item Replace $V$ by \alert{fixed} set $V'$ with $p(V')\geq (1-\alpha)p(V)$
        \item Set $\alpha=0$
    \end{itemize}
    \vspace{0.5cm}
    \textbf{Pros:}
    \begin{itemize}
        \item Better instance optimization (eliminating dominated facilities)
        \item (Soon) essential facilities provide solution headstart
    \end{itemize}
    \textbf{Cons:}
    \begin{itemize}
        \item More restriction $\Rightarrow$ worse solution quality
        \item Higher pre-processing time
    \end{itemize}
\end{frame}

\begin{frame}{Heuristics}
    \begin{center}
        \textbf{Static Reduction Heuristics}
    \end{center}
    \textbf{Anchor Deletion:}
    \begin{itemize}
        \item Delete \alert{anchors}: $v\in V$ with \alert{minimal} $|F_{d_i}(v)\cap F_i|$ for some $i$
        \item[$\Rightarrow$] More freedom for solution
    \end{itemize}

    \textbf{Area Sweep:}
    \begin{itemize}
        \item Delete locations with \alert{minimal population}
        \item[$\Rightarrow$] Maximize number of deleted locations
        \item[$\Rightarrow$] Minimize number of restrictions 
    \end{itemize}

\end{frame}

\begin{frame}{Heuristics}
    \begin{center}
        \textbf{Essential Facilities}
    \end{center}
    \begin{itemize}
        \item Only if \alert{$\alpha=0$}!
        \item $f\in F_i$ is \alert{$i$-essential} if $\exists v\in V$ with $F_{d_i}(v)\cap F_i=\{f\}$
        \item Essential facilities are \alert{guaranteed} to be part of any solution if $\alpha=0$
    \end{itemize}
    $\Rightarrow$ pre-compute essential facilities as solution-headstart

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.7,
            facility/.style={point, fill=Red!75, draw=Black, minimum width = 16pt},
            location/.style={point, color=Black, minimum width = 12pt}
        ]
            \def\r{2.6cm}
            \node[facility,label=$f_1$] (f1) at (-2,0) {};
            \node[facility,label=$f_2$] (f2) at (2,0) {};
        
            \node[location] (v1) at (-2.5,1.5) {};
            \node[location] (v2) at (-1.5,-1) {};
            \node[location] (v3) at (0,0.5) {};
            \node[location] (v4) at (0,-0.5) {};
        
            \draw[draw=Red!50] (f1) circle (\r);
            \draw[draw=Red!50] (f2) circle (\r);
        \end{tikzpicture}
    \end{figure}
    \begin{center}
        $f_1$ is essential, $f_2$ is not
    \end{center}
\end{frame}

\begin{frame}{Heuristics}
    \begin{center}
        \textbf{Multiple Levels}
    \end{center}
    \begin{itemize}
        \item $k$ greedy iterations for partial solutions $X_1,...,X_k$
        \item Modify utility function to account for multiple levels
        \item \textbf{Problem:} $f\in X_i$ may not be available for $X_{i+1}$ ($f\notin F_{i+1})$
            \\$\Rightarrow$ After fixing $X_i$, level $i+1$ might not be coverable!
    \end{itemize}
        When computing $X_i$, we have to ensure \alert{Higher Level Coverability:}
        \[p(\bigcup_{f\in \alert{X_{i}\cap F_{j}}}B_{d_{j}}(f))\geq (1-\alpha)p(V) \quad \alert{\forall j>i}\]
    
\end{frame}

\begin{frame}{Heuristics}
    \begin{center}
        \textbf{Top-Down Cover}
    \end{center}
    \begin{itemize}
        \item Approach: Cover \alert{higher levels first}
        \item Avoids the problem of higher level coverability
        \item Utility function on level $j$:
        \[\psi_j(f)=(c_j(f),...,c_1(f))\]
        where 
        \[c_j(f)=\begin{cases} |B_{d_j}(f)\cap U_j|&\text{if }f\in F_j \\ 0&\text{otherwise} \end{cases}\]
        with currently yet uncovered locations $U_j\subseteq V$
    \end{itemize}
\end{frame}
\begin{frame}{Heuristics}
    
    \begin{algorithm}[H]
        \caption{Top-Down Covering}
        \begin{algorithmic}[1]
            \For{\alert{$j=k,...,1$}}
                \While{$p(U_j)>\alpha p(V)$}
                    \State $f^*\gets\argmax_{f\in \alert{F_j}}\alert{\psi_j}(f)$
                    \State $X_i\gets X_i\cup\{f^*\}$
                \EndWhile
            \EndFor
        \end{algorithmic}
    \end{algorithm}

    \begin{itemize}
        \item \textbf{Pro:} Simple, avoids problem of higher-level coverability
        \item \textbf{Con:} Prioritizes higher levels, even when $F_1=...=F_k$ (no higher level coverability necessary)
    \end{itemize}
    
\end{frame}

\begin{frame}{Heuristics}
    \begin{center}
        \textbf{Bottom-Up Cover}
    \end{center}
    \begin{itemize}
        \item Approach: Cover \alert{lower levels first}
        \item Utility function on level $i$:
            \[\phi_i(f)=(c_i(f),...,c_k(f))\] 
        \item \alert{Extend} $X_i$ until higher level coverability is ensured
        \item Extension in a \alert{top-down fashion} using $\psi_j$
            $\Rightarrow$ Minimizes size of extension
        
    \end{itemize}
    
\end{frame}
\begin{frame}{Heuristics}
    \begin{algorithm}[H]
        \caption{Bottom-Up Covering}
        \begin{algorithmic}[1]
            \For{\alert{$i=1,...,k$}}
                \While{$p(U_i)>\alpha p(V)$}
                    \State $f^*\gets\argmax_{f\in \alert{F_i}\cap X_{i-1}}\alert{\phi_i}(f)$
                    \State $X_i\gets X_i\cup\{f^*\}$
                \EndWhile
                \For{\alert{$j=k,...,i+1$}}
                    \While{$p(U_j)>\alpha p(V)$}
                        \State $f^*\gets\argmax_{f\in \alert{F_j}\cap X_{i-1}}\alert{\psi_j}(f)$
                        \State $X_i\gets X_i\cup\{f^*\}$
                    \EndWhile
                \EndFor
            \EndFor
        \end{algorithmic}
    \end{algorithm}
    \begin{itemize}
        \item \textbf{Pro:} Prioritizes lower levels
        \item \textbf{Con:} facilities selected using $\phi_i$ unfit for higher level coverability
            \\ $\Rightarrow$ Possibly large extension necessary
    \end{itemize}
    
\end{frame}

\begin{frame}{Heuristics}
    \begin{center}
        \textbf{Balanced Cover}
    \end{center}
    \begin{itemize}
        \item Approach: Bottom-Up with \alert{iterative widening of facility pool} 
        \item Level $i$: Restrict $f^*$ to $F_j$ ($j=k,...,i$) until level $j$ covered
        \item Resembles interwoven Bottom-Up and Top-Down approaches
    \end{itemize}
    
\end{frame}
\begin{frame}{Heuristics}
    \begin{algorithm}[H]
        \caption{Balanced Covering}
        \begin{algorithmic}[1]
            \For{\alert{$i=1,...,k$}}
                \For{\alert{$j=k,...,i$}}
                    \While{$p(U_j)>\alpha p(V)$}
                        \State $f^*\gets\argmax_{f\in \alert{F_j}\cap X_{i-1}}\alert{\phi_i}(f)$
                        \State $X_i\gets X_i\cup\{f^*\}$
                    \EndWhile
                \EndFor
            \EndFor
        \end{algorithmic}
    \end{algorithm}
    \textbf{Pros:}
    \begin{itemize}
        \item Ensures higher level coverability while always prioritizing lower levels
        \item No extension
    \end{itemize}
    \textbf{Cons:}
    \begin{itemize}
        \item Restricted facility pool might lead to worse $f^*$
        \item[$\Rightarrow$] Larger $X_i$
    \end{itemize}
\end{frame}