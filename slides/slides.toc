\beamer@sectionintoc {1}{Motivation}{2}{0}{1}
\beamer@sectionintoc {2}{Problem Formulation}{5}{0}{2}
\beamer@sectionintoc {3}{Trees}{14}{0}{3}
\beamer@sectionintoc {4}{Complexity}{20}{0}{4}
\beamer@sectionintoc {5}{Instance Optimization}{22}{0}{5}
\beamer@sectionintoc {6}{Heuristics}{33}{0}{6}
\beamer@sectionintoc {7}{Evaluation}{47}{0}{7}
